export class Tor1eCompendiumAdversaires {


    async creationAdversaires() {
        console.log('===== CREATION DES ADVERSAIRES ====');

        let dossierAdversaires = await Folder.create({
            name: "Adversaires",
            type: 'Actor',
            sorting: 'a',
            parent: null
        });

        let dossierOrcsGob = await Folder.create({
            name: "Orcs et Gobelins",
            type: 'Actor',
            sorting: 'a',
            color: '#2e0505',
            parent: dossierAdversaires._id
        });
        let dossierAraignees = await Folder.create({
            name: "Araignées",
            type: 'Actor',
            sorting: 'a',
            color: '#2e0505',
            parent: dossierAdversaires._id
        });
        let dossierTrolls = await Folder.create({
            name: "Trolls",
            type: 'Actor',
            sorting: 'a',
            color: '#2e0505',
            parent: dossierAdversaires._id
        });
        let dossierLoups = await Folder.create({
            name: "Loups",
            type: 'Actor',
            sorting: 'a',
            color: '#2e0505',
            parent: dossierAdversaires._id
        });
        let dossierVampires = await Folder.create({
            name: "Vampires",
            type: 'Actor',
            sorting: 'a',
            color: '#2e0505',
            parent: dossierAdversaires._id
        });
        let dossierCreaturesDesMarais = await Folder.create({
            name: "Créatures des Marais",
            type: 'Actor',
            sorting: 'a',
            color: '#2e0505',
            parent: dossierAdversaires._id
        });
        let dossierEsprits = await Folder.create({
            name: "Spectres, Ombres et Esprits",
            type: 'Actor',
            sorting: 'a',
            color: '#2e0505',
            parent: dossierAdversaires._id
        });
        let dossierDragonsBasilics = await Folder.create({
            name: "Dragons et Basilics",
            type: 'Actor',
            sorting: 'a',
            color: '#2e0505',
            parent: dossierAdversaires._id
        });
        let dossierDivers = await Folder.create({
            name: "Diverses créatures",
            type: 'Actor',
            sorting: 'a',
            color: '#2e0505',
            parent: dossierAdversaires._id
        });        
        let dossierHommesMauvaisRhovanion = await Folder.create({
            name: "Hommes mauvais du Rhovanion",
            type: 'Actor',
            sorting: 'a',
            color: '#2e0505',
            parent: dossierAdversaires._id
        });
        let dossierHommesMauvaisEriador = await Folder.create({
            name: "Hommes mauvais d'Eriador",
            type: 'Actor',
            sorting: 'a',
            color: '#2e0505',
            parent: dossierAdversaires._id
        });
        let dossierRohan = await Folder.create({
            name: "Hommes mauvais du Rohan",
            type: 'Actor',
            sorting: 'a',
            color: '#2e0505',
            parent: dossierAdversaires._id
        });
        

        let monActor1 = await this.ajouteUnAdversaire(dossierOrcsGob, "Grand Orc", "modules/fvtt-tor1e-compendium/icons/adv-orc.webp", 7, 48, "", 8, 5, false, 3, true, 2, false, 2, true, 2, false, 2, false, 2, true, false);
        await this.ajouteArme(monActor1, "Cimeterre lourd (2m)", "", 3, true, 7, 10, 14, "Bouclier brisé", "");
        await this.ajouteArme(monActor1, "Lance à pointe large", "", 3, false, 5, 10, 12, "Perforation", "");
        await this.ajouteArme(monActor1, "Hache Orc", "", 2, true, 5, 12, 16, "Bouclier brisé", "");
        await this.ajoutePieceArmure(monActor1, "Cotte de mailles", 4, "");
        await this.ajoutePieceArmure(monActor1, "Bouclier", 2, "");
        await this.ajouteCapaciteSpeciale(monActor1, "Force effroyable", "", "");
        await this.ajouteCapaciteSpeciale(monActor1, "Voix impérieuse", "", "");
        await this.ajouteCapaciteSpeciale(monActor1, "Résistance abominable", "", "");
        await this.ajouteCapaciteSpeciale(monActor1, "Grande taille", "", "");
        let monActor2 = await this.ajouteUnAdversaire(dossierOrcsGob, "Pisteur Snaga", "modules/fvtt-tor1e-compendium/icons/adv-orc.webp", 2, 8, "", 2, 3, false, 2, false, 2, false, 2, true, 2, true, 1, false, 1, false, false);
        await this.ajouteArme(monActor2, "Arc de corne", "", 2, false, 4, 10, 12, "Poison", "");
        await this.ajouteArme(monActor2, "Couteau à dents", "", 2, true, 3, 12, 14, "", "");
        await this.ajoutePieceArmure(monActor2, "Corselet à manches longues", 2, "");
        await this.ajouteCapaciteSpeciale(monActor2, "Aversion au soleil", "", "");
        await this.ajouteCapaciteSpeciale(monActor2, "Vitesse du serpent", "", "");
        let monActor3 = await this.ajouteUnAdversaire(dossierOrcsGob, "Uruk Noir", "modules/fvtt-tor1e-compendium/icons/adv-orc.webp", 5, 20, "", 4, 5, false, 3, false, 3, true, 3, true, 2, true, 1, false, 2, false, false);
        await this.ajouteArme(monActor3, "Epée à lame large", "", 2, true, 5, 10, 14, "Poison", "");
        await this.ajouteArme(monActor3, "Lance à pointe large", "", 2, false, 5, 10, 12, "Perforation", "");
        await this.ajoutePieceArmure(monActor3, "Corselet à manches longues", 2, "");
        await this.ajoutePieceArmure(monActor3, "Bouclier", 2, "");
        await this.ajouteCapaciteSpeciale(monActor3, "Force effroyable", "", "");
        let monActor4 = await this.ajouteUnAdversaire(dossierOrcsGob, "Messager de Lugburz", "modules/fvtt-tor1e-compendium/icons/adv-orc.webp", 4, 18, "", 5, 0, false, 3, true, 2, false, 2, false, 2, true, 2, true, 3, false, false);
        await this.ajouteArme(monActor4, "Cimeterre lourd (2m)", "", 2, false, 7, 10, 14, "Bouclier brisé", "");
        await this.ajouteArme(monActor4, "Couteau à dents", "", 3, true, 3, 12, 14, "", "");
        await this.ajoutePieceArmure(monActor4, "Corselet à manches longues", 2, "");
        await this.ajouteCapaciteSpeciale(monActor4, "Aversion au soleil", "", "");
        await this.ajouteCapaciteSpeciale(monActor4, "Vitesse du serpent", "", "");
        await this.ajouteCapaciteSpeciale(monActor4, "Voix impérieuse", "", "");
        let monActor5 = await this.ajouteUnAdversaire(dossierOrcsGob, "Chef Orc", "modules/fvtt-tor1e-compendium/icons/adv-orc.webp", 5, 20, "Animosité : Mont Gram (Hobbits), Gobelinville (Nains)", 5, 4, false, 3, false, 3, true, 2, false, 2, true, 1, false, 3, true, false);
        await this.ajouteArme(monActor5, "Hache Orc", "", 3, false, 5, 12, 16, "Bouclier brisé", "");
        await this.ajouteArme(monActor5, "Lance", "", 2, true, 4, 9, 12, "Perforation", "");
        await this.ajoutePieceArmure(monActor5, "Chemise de mailles", 3, "");
        await this.ajoutePieceArmure(monActor5, "Grand bouclier", 3, "");
        await this.ajouteCapaciteSpeciale(monActor5, "Aversion au soleil", "", "");
        await this.ajouteCapaciteSpeciale(monActor5, "Voix impérieuse", "", "");
        await this.ajouteCapaciteSpeciale(monActor5, "Vitesse du serpent", "", "");
        await this.ajouteCapaciteSpeciale(monActor5, "Force effroyable", "", "");
        await this.ajouteCapaciteSpeciale(monActor5, "Animosité (culture)", "Animosité (Mont Gram : Hobbits, Gobelinville : Nains)", "");
        let monActor6 = await this.ajouteUnAdversaire(dossierOrcsGob, "Archer Gobelin", "modules/fvtt-tor1e-compendium/icons/adv-orc.webp", 2, 8, "Animosité : Mont Gram (Hobbits), Gobelinville (Nains)", 1, 2, false, 1, false, 3, true, 2, false, 2, false, 1, false, 1, false, false);
        await this.ajouteArme(monActor6, "Arc de corne", "", 2, true, 4, 10, 12, "Poison", "");
        await this.ajouteArme(monActor6, "Couteau à dents", "", 1, false, 3, 12, 14, "", "");
        await this.ajoutePieceArmure(monActor6, "Corselet à manches longues", 2, "");
        await this.ajouteCapaciteSpeciale(monActor6, "Aversion au soleil", "", "");
        await this.ajouteCapaciteSpeciale(monActor6, "Lâcheté", "", "");
        await this.ajouteCapaciteSpeciale(monActor6, "Habitant des ténèbres", "", "");
        await this.ajouteCapaciteSpeciale(monActor6, "Animosité (culture)", "Animosité (Mont Gram : Hobbits, Gobelinville : Nains)", "");
        let monActor7 = await this.ajouteUnAdversaire(dossierOrcsGob, "Garde Orc", "modules/fvtt-tor1e-compendium/icons/adv-orc.webp", 4, 16, "Animosité : Mont Gram (Hobbits), Gobelinville (Nains)", 3, 4, false, 2, false, 3, false, 3, true, 2, false, 2, true, 1, false, false);
        await this.ajouteArme(monActor7, "Lance", "", 3, false, 4, 9, 12, "Perforation", "");
        await this.ajouteArme(monActor7, "Epée courbe", "", 2, false, 4, 10, 12, "Désarmer", "");
        await this.ajoutePieceArmure(monActor7, "Corselet à manches longues", 2, "");
        await this.ajoutePieceArmure(monActor7, "Bouclier", 2, "");
        await this.ajouteCapaciteSpeciale(monActor7, "Aversion au soleil", "", "");
        await this.ajouteCapaciteSpeciale(monActor7, "Résistance abominable", "", "");
        await this.ajouteCapaciteSpeciale(monActor7, "Animosité (culture)", "Animosité (Mont Gram : Hobbits, Gobelinville : Nains)", "");
        let monActor8 = await this.ajouteUnAdversaire(dossierOrcsGob, "Soldat Orc", "modules/fvtt-tor1e-compendium/icons/adv-orc.webp", 3, 12, "Animosité : Mont Gram (Hobbits), Gobelinville (Nains)", 1, 3, false, 2, false, 2, false, 3, false, 2, true, 1, false, 1, false, false);
        await this.ajouteArme(monActor8, "Epée courbe", "", 2, false, 4, 10, 12, "Désarmer", "");
        await this.ajouteArme(monActor8, "Lance", "", 2, false, 4, 9, 12, "Perforation", "");
        await this.ajoutePieceArmure(monActor8, "Chemise de mailles", 3, "");
        await this.ajoutePieceArmure(monActor8, "Rondache", 1, "");
        await this.ajouteCapaciteSpeciale(monActor8, "Aversion au soleil", "", "");
        await this.ajouteCapaciteSpeciale(monActor8, "Lâcheté", "", "");
        await this.ajouteCapaciteSpeciale(monActor8, "Animosité (culture)", "Animosité (Mont Gram : Hobbits, Gobelinville : Nains)", "");
        let monActor9 = await this.ajouteUnAdversaire(dossierOrcsGob, "Gorgol, fils de Bolg", "modules/fvtt-tor1e-compendium/icons/adv-orc.webp", 6, 45, "Cimeterre d’Azog : Héritage des Gobelins du Nord. Augmente la valeur de Haine de son porteur de 3 et de ses suivants de 1.", 10, 6, false, 4, true, 2, false, 3, true, 2, false, 2, false, 4, true, true);
        await this.ajouteArme(monActor9, "Cimeterre lourd (2m)", "", 4, true, 8, 10, 15, "Bouclier brisé, Perforation", "<b>Cimeterre d’Azog</b> : Héritage des Gobelins du Nord. Augmente la valeur de Haine de son porteur de 3 et de ses suivants de 1.");
        await this.ajouteArme(monActor9, "Hache Orc", "", 2, false, 5, 12, 16, "Perforation", "");
        await this.ajoutePieceArmure(monActor9, "Cotte de mailles", 4, "");
        await this.ajouteCapaciteSpeciale(monActor9, "Force effroyable", "", "");
        await this.ajouteCapaciteSpeciale(monActor9, "Grande taille", "", "");
        await this.ajouteCapaciteSpeciale(monActor9, "Voix impérieuse", "", "");
        await this.ajouteCapaciteSpeciale(monActor9, "Animosité (culture)", "Animosité (Nains)", "");
        let monActor10 = await this.ajouteUnAdversaire(dossierOrcsGob, "Nouveau Grand Gobelin", "modules/fvtt-tor1e-compendium/icons/adv-orc.webp", 7, 60, "", 8, 3, false, 3, true, 1, false, 2, false, 2, false, 3, true, 3, true, true);
        await this.ajouteArme(monActor10, "Hache Orc", "", 3, false, 5, 12, 16, "Bouclier brisé", "");
        await this.ajouteArme(monActor10, "Lance à pointe large", "", 2, false, 5, 10, 12, "Perforation", "");
        await this.ajoutePieceArmure(monActor10, "Cotte de mailles", 4, "");
        await this.ajoutePieceArmure(monActor10, "Grand bouclier", 3, "");
        await this.ajouteCapaciteSpeciale(monActor10, "Résistance abominable", "", "");
        await this.ajouteCapaciteSpeciale(monActor10, "Grande taille", "", "");
        await this.ajouteCapaciteSpeciale(monActor10, "Lâcheté", "", "");
        await this.ajouteCapaciteSpeciale(monActor10, "Animosité (culture)", "Animosité (Beornides)", "");
        let monActor11 = await this.ajouteUnAdversaire(dossierOrcsGob, "Maghaz, Capitaine Orc", "modules/fvtt-tor1e-compendium/icons/adv-orc.webp", 4, 14, "", 5, 3, false, 3, true, 1, false, 2, false, 2, true, 2, false, 3, false, true);
        await this.ajouteArme(monActor11, "Epée courbe", "", 3, false, 4, 10, 12, "Désarmer", "");
        await this.ajouteArme(monActor11, "Lance", "", 2, false, 4, 9, 12, "Perforation", "");
        await this.ajoutePieceArmure(monActor11, "Chemise de mailles", 3, "");
        await this.ajoutePieceArmure(monActor11, "Rondache", 1, "");
        await this.ajouteCapaciteSpeciale(monActor11, "Aversion au soleil", "", "");
        await this.ajouteCapaciteSpeciale(monActor11, "Grande taille", "", "");
        await this.ajouteCapaciteSpeciale(monActor11, "Vitesse du serpent", "", "");
        let monActor12 = await this.ajouteUnAdversaire(dossierOrcsGob, "Gobelin de la Forêt", "modules/fvtt-tor1e-compendium/icons/adv-orc.webp", 2, 10, "", 2, 1, false, 1, false, 3, true, 2, false, 2, true, 1, false, 1, false, false);
        await this.ajouteArme(monActor12, "Lance à tête de pierre", "", 2, true, 4, 10, 12, "Perforation", "");
        await this.ajoutePieceArmure(monActor12, "Corselet à manches longues", 2, "");
        await this.ajoutePieceArmure(monActor12, "Rondache", 1, "");
        await this.ajouteCapaciteSpeciale(monActor12, "Aversion au soleil", "", "");
        await this.ajouteCapaciteSpeciale(monActor12, "Lâcheté", "", "");
        await this.ajouteCapaciteSpeciale(monActor12, "Force effroyable", "", "");
        await this.ajouteCapaciteSpeciale(monActor12, "Habitant de la Forêt Noire", "", "");
        let monActor13 = await this.ajouteUnAdversaire(dossierOrcsGob, "Ghor l’Aigrefin", "modules/fvtt-tor1e-compendium/icons/adv-orc.webp", 6, 35, "", 6, 5, true, 3, true, 2, false, 2, true, 2, false, 2, false, 2, false, true);
        await this.ajouteArme(monActor13, "Cimeterre lourd (2m)", "", 3, true, 7, 10, 14, "Bouclier brisé", "");
        await this.ajouteArme(monActor13, "Hache Orc", "", 3, false, 5, 12, 16, "Bouclier brisé", "");
        await this.ajoutePieceArmure(monActor13, "Chemise de mailles", 3, "");
        await this.ajoutePieceArmure(monActor13, "Heaume", 4, "");
        await this.ajouteCapaciteSpeciale(monActor13, "Voix impérieuse", "", "");
        await this.ajouteCapaciteSpeciale(monActor13, "Force effroyable", "", "");
        await this.ajouteCapaciteSpeciale(monActor13, "Résistance abominable", "", "");
        await this.ajouteCapaciteSpeciale(monActor13, "Assaut brutal", "", "");
        let monActor14 = await this.ajouteUnAdversaire(dossierOrcsGob, "Burzash, Seigneur de Guerre", "modules/fvtt-tor1e-compendium/icons/adv-orc.webp", 7, 48, "", 8, 5, false, 3, false, 2, false, 2, true, 2, false, 2, false, 2, true, true);
        await this.ajouteArme(monActor14, "Cimeterre lourd (2m)", "", 3, true, 7, 10, 14, "Bouclier brisé", "");
        await this.ajouteArme(monActor14, "Lance à pointe large", "", 3, false, 5, 10, 12, "Perforation", "");
        await this.ajouteArme(monActor14, "Hache Orc", "", 2, true, 5, 12, 16, "Bouclier brisé", "");
        await this.ajoutePieceArmure(monActor14, "Bouclier", 2, "");
        await this.ajoutePieceArmure(monActor14, "Cotte de mailles", 4, "");
        await this.ajouteCapaciteSpeciale(monActor14, "Voix impérieuse", "", "");
        await this.ajouteCapaciteSpeciale(monActor14, "Force effroyable", "", "");
        await this.ajouteCapaciteSpeciale(monActor14, "Résistance abominable", "", "");
        await this.ajouteCapaciteSpeciale(monActor14, "Grande taille", "", "");
        let monActor15 = await this.ajouteUnAdversaire(dossierOrcsGob, "Gobelin de Carn Dûm", "modules/fvtt-tor1e-compendium/icons/adv-orc.webp", 2, 10, "<b>Bande disparate</b> : Si des Hommes des Collines accompagnent les Gobelins, la Lâcheté est inopérante et le SR pour tendre une embuscade passe à 18.", 2, 3, false, 1, false, 3, false, 3, false, 2, true, 1, false, 1, true, false);
        await this.ajouteArme(monActor15, "Epée à lame large", "", 2, true, 5, 10, 14, "Poison", "");
        await this.ajouteArme(monActor15, "Arc de corne", "", 1, false, 4, 10, 12, "Poison", "");
        await this.ajoutePieceArmure(monActor15, "Corselet à manches longues", 2, "");
        await this.ajouteCapaciteSpeciale(monActor15, "Lâcheté", "", "");
        await this.ajouteCapaciteSpeciale(monActor15, "Aversion au soleil", "", "");
        await this.ajouteCapaciteSpeciale(monActor15, "Animosité (culture)", "Animosité (Dunedains)", "");
        await this.ajouteCapaciteSpeciale(monActor15, "Animosité (culture)", "Animosité (Elfes)", "");
        let monActor16 = await this.ajouteUnAdversaire(dossierOrcsGob, "Orc du Mont Gram", "modules/fvtt-tor1e-compendium/icons/adv-orc.webp", 4, 14, "Les Orcs de Gram n’ont pas oublié la défaite cuisante infligée par Bandobras Touque, le Taureau Mugissant, et vouent depuis une haine féroce à tous les Hobbits. Leur ambition les amène aussi à conspirer contre leurs voisins Orcs et à mener une patrouille de Rôdeurs vers le site d’une communauté orque rivale.", 4, 3, false, 3, false, 2, false, 2, true, 2, true, 1, false, 1, false, false);
        await this.ajouteArme(monActor16, "Epée courbe", "", 2, true, 4, 10, 12, "Désarmer", "");
        await this.ajouteArme(monActor16, "Lance", "", 2, false, 4, 9, 14, "Perforation", "");
        await this.ajoutePieceArmure(monActor16, "Chemise de mailles", 3, "");
        await this.ajoutePieceArmure(monActor16, "Rondache", 1, "");
        await this.ajouteCapaciteSpeciale(monActor16, "Lâcheté", "", "");
        await this.ajouteCapaciteSpeciale(monActor16, "Aversion au soleil", "", "");
        await this.ajouteCapaciteSpeciale(monActor16, "Animosité (culture)", "Animosité (Hobbits)", "");
        let monActor17 = await this.ajouteUnAdversaire(dossierOrcsGob, "Radgul le chef Orc", "modules/fvtt-tor1e-compendium/icons/adv-orc.webp", 6, 35, "Meneur des Orcs du Mont Gram. Vieil Orc balafré, noueux et ambitieux qui vise le trône du Mont Gundabad en jouant de sa patience et de sa ruse.", 9, 6, false, 4, true, 2, true, 3, false, 3, true, 2, false, 2, false, true);
        await this.ajouteArme(monActor17, "Hache Orc", "", 4, false, 5, 12, 16, "Bouclier brisé", "");
        await this.ajouteArme(monActor17, "Lance", "", 2, true, 4, 9, 12, "Perforation", "");
        await this.ajoutePieceArmure(monActor17, "Cotte de mailles", 4, "");
        await this.ajoutePieceArmure(monActor17, "Grand bouclier", 3, "");
        await this.ajouteCapaciteSpeciale(monActor17, "Voix impérieuse", "", "");
        await this.ajouteCapaciteSpeciale(monActor17, "Grande taille", "", "");
        await this.ajouteCapaciteSpeciale(monActor17, "Force effroyable", "", "");
        await this.ajouteCapaciteSpeciale(monActor17, "Animosité (culture)", "Animosité (Hobbits)", "");
        let monActor18 = await this.ajouteUnAdversaire(dossierOrcsGob, "Gishak Gashnaga", "modules/fvtt-tor1e-compendium/icons/adv-orc.webp", 7, 48, "", 8, 5, false, 3, true, 2, false, 2, true, 2, false, 2, false, 2, true, true);
        await this.ajouteArme(monActor18, "Cimeterre lourd (2m)", "", 3, true, 7, 10, 14, "Bouclier brisé", "");
        await this.ajouteArme(monActor18, "Lance à pointe large", "", 3, false, 5, 10, 12, "Perforation", "");
        await this.ajouteArme(monActor18, "Hache Orc", "", 2, true, 5, 12, 16, "Bouclier brisé", "");
        await this.ajoutePieceArmure(monActor18, "Cotte de mailles", 4, "");
        await this.ajoutePieceArmure(monActor18, "Bouclier", 2, "");
        await this.ajouteCapaciteSpeciale(monActor18, "Force effroyable", "", "");
        await this.ajouteCapaciteSpeciale(monActor18, "Voix impérieuse", "", "");
        await this.ajouteCapaciteSpeciale(monActor18, "Résistance abominable", "", "");
        await this.ajouteCapaciteSpeciale(monActor18, "Grande taille", "", "");
        let monActor19 = await this.ajouteUnAdversaire(dossierOrcsGob, "L’Impitoyable", "modules/fvtt-tor1e-compendium/icons/adv-orc.webp", 7, 48, "<b>Lieu</b>: Tour de la Flamme de Glace, Vallée Glacée (Nan Gorthrim), Angmar", 8, 5, false, 3, true, 2, false, 3, true, 2, false, 2, false, 3, true, true);
        await this.ajouteArme(monActor19, "Cimeterre lourd (2m)", "", 4, true, 7, 10, 14, "Bouclier brisé", "");
        await this.ajouteArme(monActor19, "Lance à pointe large", "", 3, false, 5, 10, 12, "Perforation", "");
        await this.ajoutePieceArmure(monActor19, "Cotte de mailles", 4, "");
        await this.ajoutePieceArmure(monActor19, "Bouclier", 2, "");
        await this.ajouteCapaciteSpeciale(monActor19, "Force effroyable", "", "");
        await this.ajouteCapaciteSpeciale(monActor19, "Voix impérieuse", "", "");
        await this.ajouteCapaciteSpeciale(monActor19, "Résistance abominable", "", "");
        await this.ajouteCapaciteSpeciale(monActor19, "Grande taille", "", "");
        await this.ajouteCapaciteSpeciale(monActor19, "Insaisissable", "", "");
        await this.ajouteCapaciteSpeciale(monActor19, "Meurtrier", "", "");
        let monActor20 = await this.ajouteUnAdversaire(dossierAraignees, "Attercop", "modules/fvtt-tor1e-compendium/icons/adv-araignee.webp", 3, 12, "", 2, 4, false, 1, false, 3, true, 1, true, 1, false, 1, false, 1, false, false);
        await this.ajouteArme(monActor20, "Toile", "", 2, true, 0, 0, 0, "", "");
        await this.ajouteArme(monActor20, "Dard", "", 2, true, 3, 10, 14, "Poison", "");
        await this.ajoutePieceArmure(monActor20, "Corselet à manches longues", 2, "Carapace");
        await this.ajouteCapaciteSpeciale(monActor20, "Grand bond", "", "");
        await this.ajouteCapaciteSpeciale(monActor20, "Etreinte", "", "");
        let monActor21 = await this.ajouteUnAdversaire(dossierAraignees, "Araignée des Cavernes", "modules/fvtt-tor1e-compendium/icons/adv-araignee.webp", 3, 12, "", 2, 4, false, 1, false, 3, true, 1, true, 1, false, 1, false, 1, false, false);
        await this.ajouteArme(monActor21, "Toile", "", 2, true, 0, 0, 0, "", "");
        await this.ajouteArme(monActor21, "Dard", "", 2, true, 3, 10, 14, "Poison", "");
        await this.ajoutePieceArmure(monActor21, "Corselet à manches longues", 2, "Carapace");
        await this.ajouteCapaciteSpeciale(monActor21, "Habitant des ténèbres", "", "");
        await this.ajouteCapaciteSpeciale(monActor21, "Etreinte", "", "");
        let monActor22 = await this.ajouteUnAdversaire(dossierAraignees, "Grande Araignée", "modules/fvtt-tor1e-compendium/icons/adv-araignee.webp", 4, 36, "", 3, 5, false, 3, false, 3, true, 2, true, 2, false, 2, false, 1, false, false);
        await this.ajouteArme(monActor22, "Toile", "", 2, true, 0, 0, 0, "", "");
        await this.ajouteArme(monActor22, "Dard", "", 2, true, 3, 10, 14, "Poison", "");
        await this.ajoutePieceArmure(monActor22, "Corselet à manches longues", 3, "Carapace");
        await this.ajouteCapaciteSpeciale(monActor22, "Effroi", "", "");
        await this.ajouteCapaciteSpeciale(monActor22, "Epouvantables sortilèges", "", "Envoutement : Test Corruption raté = héro ne peut attaquer pendant (10-Sagesse) rounds.");
        await this.ajouteCapaciteSpeciale(monActor22, "Habitant des ténèbres", "", "");
        await this.ajouteCapaciteSpeciale(monActor22, "Etreinte", "", "");
        let monActor23 = await this.ajouteUnAdversaire(dossierAraignees, "Araignée chasseresse", "modules/fvtt-tor1e-compendium/icons/adv-araignee.webp", 4, 25, "", 3, 6, false, 2, false, 3, true, 2, false, 3, true, 1, false, 1, false, false);
        await this.ajouteArme(monActor23, "Crochets", "", 2, true, 6, 12, 15, "Poison", "");
        await this.ajoutePieceArmure(monActor23, "Corselet à manches longues", 3, "Carapace");
        await this.ajouteCapaciteSpeciale(monActor23, "Grand bond", "", "");
        await this.ajouteCapaciteSpeciale(monActor23, "Force effroyable", "", "");
        let monActor24 = await this.ajouteUnAdversaire(dossierAraignees, "Sarqin la grasse", "modules/fvtt-tor1e-compendium/icons/adv-araignee.webp", 8, 90, "", 8, 5, true, 4, false, 1, true, 3, false, 2, true, 3, false, 2, false, true);
        await this.ajouteArme(monActor24, "Toile", "", 3, true, 0, 0, 0, "", "Si succès supérieur + : attaque immédiate");
        await this.ajouteArme(monActor24, "Crochets", "", 4, true, 6, 12, 15, "Poison", "<b>Poison d’araignée supérieur</b> : Durée 1D6 jours.");
        await this.ajoutePieceArmure(monActor24, "Chemise de mailles", 3, "Carapace");
        await this.ajouteCapaciteSpeciale(monActor24, "Grande taille", "", "");
        await this.ajouteCapaciteSpeciale(monActor24, "Cuir robuste", "", "");
        await this.ajouteCapaciteSpeciale(monActor24, "Abomination", "", "<b>Sarqin la grasse</b> : utiliser un SR 16");
        await this.ajouteCapaciteSpeciale(monActor24, "Etreinte", "", "");
        await this.ajouteCapaciteSpeciale(monActor24, "Puanteur fétide", "", "");
        await this.ajouteCapaciteSpeciale(monActor24, "Enfants innombrables", "", "");
        let monActor25 = await this.ajouteUnAdversaire(dossierAraignees, "Tauler le sauvage", "modules/fvtt-tor1e-compendium/icons/adv-araignee.webp", 7, 60, "", 8, 8, true, 3, false, 4, true, 4, false, 4, true, 3, false, 3, false, true);
        await this.ajouteArme(monActor25, "Crochets", "", 5, true, 7, 12, 14, "Poison", "<b>Poison d’araignée supérieur</b> : Durée 1D6 jours.");
        await this.ajouteArme(monActor25, "Piétinement", "", 3, false, 7, 8, 18, "Renversement", "");
        await this.ajoutePieceArmure(monActor25, "Chemise de mailles", 3, "Carapace");
        await this.ajouteCapaciteSpeciale(monActor25, "Grande taille", "", "");
        await this.ajouteCapaciteSpeciale(monActor25, "Force effroyable", "", "");
        await this.ajouteCapaciteSpeciale(monActor25, "Résistance abominable", "", "<b>Tauler le sauvage</b> : utiliser un SR 16");
        await this.ajouteCapaciteSpeciale(monActor25, "Effroi", "", "<b>Tauler le sauvage</b> : utiliser un SR 16");
        let monActor26 = await this.ajouteUnAdversaire(dossierAraignees, "Tyulquin la tisseuse", "modules/fvtt-tor1e-compendium/icons/adv-araignee.webp", 9, 60, "", 8, 7, true, 4, false, 3, true, 3, false, 3, true, 2, false, 4, true, true);
        await this.ajouteArme(monActor26, "Toile", "", 3, true, 0, 0, 0, "", "Si succès supérieur + : attaque immédiate");
        await this.ajouteArme(monActor26, "Crochets", "", 4, false, 9, 8, 18, "Poison", "<b>Poison d’araignée supérieur</b> : Durée 1D6 jours.");
        await this.ajoutePieceArmure(monActor26, "Chemise de mailles", 3, "Carapace");
        await this.ajouteCapaciteSpeciale(monActor26, "Grande taille", "", "");
        await this.ajouteCapaciteSpeciale(monActor26, "Effroi", "", "<b>Tyulqin la tisseuse</b> : utiliser un SR 20");
        await this.ajouteCapaciteSpeciale(monActor26, "Epouvantables sortilèges", "", "<b>Hébètement</b> : La cible fait un test de Corruption SR 16. Echec : sous l’influence de l’araignée et avance vers la toile la plus proche. Capturé (Etreinte). Prochaine action perdue.");
        await this.ajouteCapaciteSpeciale(monActor26, "Etreinte", "", "");
        await this.ajouteCapaciteSpeciale(monActor26, "Nombreux poisons", "", "");
        await this.ajouteCapaciteSpeciale(monActor26, "Toiles d’illusion", "", "");
        let monActor27 = await this.ajouteUnAdversaire(dossierTrolls, "Troll des cavernes", "modules/fvtt-tor1e-compendium/icons/adv-troll.webp", 7, 76, "", 8, 5, true, 2, false, 2, true, 1, true, 2, false, 0, false, 0, false, false);
        await this.ajoutePieceArmure(monActor27, "Chemise de mailles", 3, "Cuir épais");
        await this.ajouteArme(monActor27, "Morsure", "", 3, false, 5, 12, 14, "", "");
        await this.ajouteArme(monActor27, "Ecrasement", "", 1, true, 7, 12, 12, "", "");
        await this.ajouteCapaciteSpeciale(monActor27, "Grande taille", "", "");
        await this.ajouteCapaciteSpeciale(monActor27, "Assaut brutal", "", "");
        await this.ajouteCapaciteSpeciale(monActor27, "Résistance abominable", "", "");
        await this.ajouteCapaciteSpeciale(monActor27, "Cuir robuste", "", "");
        let monActor28 = await this.ajouteUnAdversaire(dossierTrolls, "Troll des collines", "modules/fvtt-tor1e-compendium/icons/adv-troll.webp", 7, 84, "", 7, 5, false, 3, false, 2, false, 3, false, 2, false, 0, false, 2, false, false);
        await this.ajoutePieceArmure(monActor28, "Chemise de mailles", 3, "Cuir épais");
        await this.ajoutePieceArmure(monActor28, "Rondache", 1, "");
        await this.ajouteArme(monActor28, "Marteau lourd", "", 3, false, 8, 12, 16, "Bouclier brisé", "");
        await this.ajouteArme(monActor28, "Ecrasement", "", 2, false, 7, 12, 12, "", "");
        await this.ajouteCapaciteSpeciale(monActor28, "Grande taille", "", "");
        await this.ajouteCapaciteSpeciale(monActor28, "Résistance abominable", "", "");
        await this.ajouteCapaciteSpeciale(monActor28, "Effroi", "", "");
        let monActor29 = await this.ajouteUnAdversaire(dossierTrolls, "Chef troll des collines", "modules/fvtt-tor1e-compendium/icons/adv-troll.webp", 8, 90, "", 10, 6, false, 3, true, 2, false, 2, false, 2, false, 1, false, 3, false, false);
        await this.ajoutePieceArmure(monActor29, "Chemise de mailles", 4, "Cuir épais");
        await this.ajoutePieceArmure(monActor29, "Rondache", 1, "");
        await this.ajouteArme(monActor29, "Marteau lourd", "", 4, true, 8, 12, 16, "Bouclier brisé", "");
        await this.ajouteArme(monActor29, "Ecrasement", "", 2, true, 5, 12, 14, "", "");
        await this.ajouteCapaciteSpeciale(monActor29, "Grande taille", "", "");
        await this.ajouteCapaciteSpeciale(monActor29, "Résistance abominable", "", "");
        await this.ajouteCapaciteSpeciale(monActor29, "Force effroyable", "", "");
        await this.ajouteCapaciteSpeciale(monActor29, "Pas de quartier", "", "");
        let monActor30 = await this.ajouteUnAdversaire(dossierTrolls, "Troll des montagnes", "modules/fvtt-tor1e-compendium/icons/adv-troll.webp", 9, 96, "", 7, 7, false, 4, true, 2, false, 2, false, 3, false, 1, false, 1, false, false);
        await this.ajoutePieceArmure(monActor30, "Chemise de mailles", 4, "Cuir épais");
        await this.ajouteArme(monActor30, "Ecrasement", "", 4, true, 9, 12, 12, "", "");
        await this.ajouteCapaciteSpeciale(monActor30, "Grande taille", "", "");
        await this.ajouteCapaciteSpeciale(monActor30, "Assaut brutal", "", "");
        await this.ajouteCapaciteSpeciale(monActor30, "Force effroyable", "", "");
        await this.ajouteCapaciteSpeciale(monActor30, "Abomination", "", "");
        let monActor31 = await this.ajouteUnAdversaire(dossierTrolls, "Troll de pierre", "modules/fvtt-tor1e-compendium/icons/adv-troll.webp", 6, 72, "", 5, 5, false, 2, false, 2, false, 2, true, 1, false, 1, true, 1, false, false);
        await this.ajouteArme(monActor31, "Massue", "", 3, false, 6, 10, 14, "", "");
        await this.ajouteArme(monActor31, "Ecrasement", "", 1, true, 6, 12, 12, "", "");
        await this.ajoutePieceArmure(monActor31, "Chemise de mailles", 3, "Pierre");
        await this.ajouteCapaciteSpeciale(monActor31, "Grande taille", "", "");
        await this.ajouteCapaciteSpeciale(monActor31, "Animosité (culture)", "Animosité (Nains)", "");
        await this.ajouteCapaciteSpeciale(monActor31, "Force effroyable", "", "");
        let monActor32 = await this.ajouteUnAdversaire(dossierTrolls, "Troll de pierre affaibli", "modules/fvtt-tor1e-compendium/icons/adv-troll.webp", 6, 60, "", 3, 5, false, 2, false, 2, false, 1, true, 1, false, 1, true, 1, false, false);
        await this.ajouteArme(monActor32, "Massue", "", 3, false, 6, 10, 14, "", "");
        await this.ajouteArme(monActor32, "Ecrasement", "", 1, true, 6, 12, 12, "", "");
        await this.ajoutePieceArmure(monActor32, "Chemise de mailles", 3, "Pierre");
        await this.ajouteCapaciteSpeciale(monActor32, "Grande taille", "", "");
        await this.ajouteCapaciteSpeciale(monActor32, "Animosité (culture)", "Animosité (Nains)", "");
        await this.ajouteCapaciteSpeciale(monActor32, "Force effroyable", "", "");
        let monActor33 = await this.ajouteUnAdversaire(dossierTrolls, "Troll des neiges", "modules/fvtt-tor1e-compendium/icons/adv-troll.webp", 8, 80, "", 8, 6, true, 3, false, 3, true, 2, false, 2, true, 0, false, 0, false, false);
        await this.ajouteArme(monActor33, "Ecrasement", "", 3, true, 5, 10, 16, "", "");
        await this.ajouteArme(monActor33, "Morsure", "", 4, false, 8, 12, 14, "", "");
        await this.ajoutePieceArmure(monActor33, "Chemise de mailles", 3, "Cuir épais");
        await this.ajouteCapaciteSpeciale(monActor33, "Grande taille", "", "");
        await this.ajouteCapaciteSpeciale(monActor33, "Force effroyable", "", "");
        await this.ajouteCapaciteSpeciale(monActor33, "Etreinte", "", "");
        await this.ajouteCapaciteSpeciale(monActor33, "Effroi", "", "");
        let monActor34 = await this.ajouteUnAdversaire(dossierTrolls, "Berk le portier", "modules/fvtt-tor1e-compendium/icons/adv-troll.webp", 6, 72, "Berk vit en bordure de la route de l’est où il aime piéger les voyageurs en mettant des troncs d’arbre sur la route pour les forcer à déblayer et se fatiguer. Lors de la nuit, il peut alors tranquillement saccager le camp des endormis. Il habite un trou de troll qui hébergeait auparavant 3 trolls aujourd’hui pétrifiés", 5, 5, false, 2, false, 2, false, 2, true, 1, false, 1, true, 1, false, true);
        await this.ajouteArme(monActor34, "Massue", "", 3, false, 6, 10, 14, "", "");
        await this.ajouteArme(monActor34, "Ecrasement", "", 1, true, 6, 12, 12, "", "");
        await this.ajoutePieceArmure(monActor34, "Chemise de mailles", 3, "Cuir épais");
        await this.ajouteCapaciteSpeciale(monActor34, "Grande taille", "", "");
        await this.ajouteCapaciteSpeciale(monActor34, "Animosité (culture)", "Animosité (Nains)", "");
        await this.ajouteCapaciteSpeciale(monActor34, "Force effroyable", "", "");
        let monActor35 = await this.ajouteUnAdversaire(dossierTrolls, "Moignon-sanglant le chasseur", "modules/fvtt-tor1e-compendium/icons/adv-troll.webp", 8, 95, "", 10, 4, false, 3, false, 2, false, 2, false, 3, true, 1, false, 3, false, true);
        await this.ajouteArme(monActor35, "Grande masse noire", "", 4, true, 8, 10, 14, "Bouclier brisé", "");
        await this.ajouteArme(monActor35, "Morsure", "", 2, true, 5, 8, 14, "", "");
        await this.ajoutePieceArmure(monActor35, "Cotte de mailles", 4, "Cuir très épais");
        await this.ajouteCapaciteSpeciale(monActor35, "Grande taille", "", "");
        await this.ajouteCapaciteSpeciale(monActor35, "Animosité (culture)", "Animosité (Rôdeurs du Nord)", "");
        await this.ajouteCapaciteSpeciale(monActor35, "Force effroyable", "", "");
        await this.ajouteCapaciteSpeciale(monActor35, "Résistance abominable", "", "");
        await this.ajouteCapaciteSpeciale(monActor35, "Coup impitoyable", "", "");
        let monActor36 = await this.ajouteUnAdversaire(dossierTrolls, "Ettin", "modules/fvtt-tor1e-compendium/icons/adv-troll.webp", 6, 76, "", 7, 3, true, 1, false, 0, false, 3, false, 1, true, 0, false, 0, false, false);
        await this.ajouteArme(monActor36, "Ecrasement", "", 3, true, 6, 12, 12, "", "");
        await this.ajouteArme(monActor36, "Déchiquetage", "", 2, false, 6, 12, 16, "", "");
        await this.ajoutePieceArmure(monActor36, "Chemise de mailles", 3, "Cuir épais");
        await this.ajouteCapaciteSpeciale(monActor36, "Grande taille", "", "");
        await this.ajouteCapaciteSpeciale(monActor36, "Force effroyable", "", "");
        await this.ajouteCapaciteSpeciale(monActor36, "Etreinte", "", "");
        await this.ajouteCapaciteSpeciale(monActor36, "Bicéphale", "Bicephale ou 4 bras", "Bicéphale : si un héros tend une embuscade, il lance 2 Dés du Destin à son jet d'Art de la Guerre, Chasse ou Discrétion et garde le résultat le plus faible. De plus tous les jets de Discrétion d'un héros a un SR +2. 4 bras : Deux cibles en utilisant Etreinte, ou Une cible + 1 attaque principale.");
        let monActor37 = await this.ajouteUnAdversaire(dossierTrolls, "Rine, la Reine de Costefort", "modules/fvtt-tor1e-compendium/icons/adv-troll.webp", 7, 80, "", 6, 3, false, 3, true, 2, false, 2, false, 2, true, 2, true, 3, false, true);
        await this.ajouteArme(monActor37, "Massue", "", 4, false, 6, 10, 14, "", "");
        await this.ajouteArme(monActor37, "Ecrasement", "", 2, true, 7, 12, 12, "Renversement", "");
        await this.ajoutePieceArmure(monActor37, "Chemise de mailles", 3, "Cuir épais");
        await this.ajouteCapaciteSpeciale(monActor37, "Grande taille", "", "");
        await this.ajouteCapaciteSpeciale(monActor37, "Animosité (culture)", "Animosité (Nains)", "");
        await this.ajouteCapaciteSpeciale(monActor37, "Force effroyable", "", "");
        await this.ajouteCapaciteSpeciale(monActor37, "Point faible", "", "Quand la créature fait un coup précis ou dépense un point de haine, elle expose son point faible pour la prochaine attaque des compagnons. Si elle est alors touché par un coup performant, son test de Protection est effectué avec la valeur réduite d’Armure (valeur avec * dans son profil).");
        let monActor38 = await this.ajouteUnAdversaire(dossierTrolls, "Capitaine Mormog", "modules/fvtt-tor1e-compendium/icons/adv-troll.webp", 9, 95, "", 10, 3, false, 4, true, 3, false, 3, true, 3, false, 2, false, 3, false, true);
        await this.ajouteArme(monActor38, "Epée dentelée", "", 3, true, 8, 12, 16, "Perforation", "");
        await this.ajouteArme(monActor38, "Griffes", "", 3, true, 6, 12, 12, "Désarmer", "");
        await this.ajoutePieceArmure(monActor38, "Chemise de mailles", 3, "Cuir épais");
        await this.ajouteCapaciteSpeciale(monActor38, "Grande taille", "", "");
        await this.ajouteCapaciteSpeciale(monActor38, "Résistance abominable", "", "");
        await this.ajouteCapaciteSpeciale(monActor38, "Force effroyable", "", "");
        await this.ajouteCapaciteSpeciale(monActor38, "Terrible", "", "");
        await this.ajouteCapaciteSpeciale(monActor38, "Vitesse du serpent", "", "");
        let monActor39 = await this.ajouteUnAdversaire(dossierLoups, "Warg", "modules/fvtt-tor1e-compendium/icons/adv-loup.webp", 3, 12, "", 1, 5, false, 1, false, 3, false, 2, true, 3, true, 0, false, 0, false, false);
        await this.ajoutePieceArmure(monActor39, "Corselet à manches longues", 2, "Cuir épais");
        await this.ajouteArme(monActor39, "Morsure", "", 2, true, 3, 10, 14, "Perforation", "");
        await this.ajouteCapaciteSpeciale(monActor39, "Peur du feu", "", "");
        await this.ajouteCapaciteSpeciale(monActor39, "Etreinte", "", "");
        await this.ajouteCapaciteSpeciale(monActor39, "Grand bond", "", "");
        let monActor40 = await this.ajouteUnAdversaire(dossierLoups, "Chef de meute", "modules/fvtt-tor1e-compendium/icons/adv-loup.webp", 5, 16, "", 3, 6, false, 2, false, 3, false, 2, false, 3, false, 1, false, 1, true, false);
        await this.ajoutePieceArmure(monActor40, "Chemise de mailles", 3, "Cuir épais");
        await this.ajouteArme(monActor40, "Morsure", "", 3, false, 5, 10, 14, "Perforation", "");
        await this.ajouteArme(monActor40, "Déchiquetage", "", 1, true, 5, 12, 12, "", "");
        await this.ajouteCapaciteSpeciale(monActor40, "Peur du feu", "", "");
        await this.ajouteCapaciteSpeciale(monActor40, "Effroi", "", "");
        await this.ajouteCapaciteSpeciale(monActor40, "Assaut brutal", "", "");
        let monActor41 = await this.ajouteUnAdversaire(dossierLoups, "Molosse de Sauron", "modules/fvtt-tor1e-compendium/icons/adv-loup.webp", 6, 20, "", 5, 6, false, 3, false, 3, true, 3, true, 2, true, 1, false, 3, false, false);
        await this.ajoutePieceArmure(monActor41, "Chemise de mailles", 3, "Cuir épais");
        await this.ajouteArme(monActor41, "Morsure", "", 3, true, 6, 10, 14, "Perforation", "");
        await this.ajouteArme(monActor41, "Déchiquetage", "", 1, true, 6, 12, 12, "", "");
        await this.ajouteCapaciteSpeciale(monActor41, "Effroi", "", "");
        await this.ajouteCapaciteSpeciale(monActor41, "Résistance abominable", "", "");
        await this.ajouteCapaciteSpeciale(monActor41, "Assaut brutal", "", "");
        let monActor42 = await this.ajouteUnAdversaire(dossierLoups, "Loup Garou de la Forêt Noire", "modules/fvtt-tor1e-compendium/icons/adv-loup.webp", 8, 68, "", 12, 9, false, 2, true, 3, true, 3, false, 3, true, 1, false, 3, false, true);
        await this.ajoutePieceArmure(monActor42, "Cotte de mailles", 4, "Cuir très épais");
        await this.ajouteArme(monActor42, "Morsure", "", 4, true, 8, 10, 14, "Perforation", "");
        await this.ajouteArme(monActor42, "Déchiquetage", "", 1, true, 8, 12, 12, "", "");
        await this.ajouteCapaciteSpeciale(monActor42, "Abomination", "Abomination (SR16)", "");
        await this.ajouteCapaciteSpeciale(monActor42, "Assaut brutal", "", "");
        await this.ajouteCapaciteSpeciale(monActor42, "Habitant des ténèbres", "", "");
        await this.ajouteCapaciteSpeciale(monActor42, "Grande taille", "", "");
        await this.ajouteCapaciteSpeciale(monActor42, "Grand bond", "", "");
        let monActor43 = await this.ajouteUnAdversaire(dossierLoups, "Nagrhaw, Chef de meute Warg", "modules/fvtt-tor1e-compendium/icons/adv-loup.webp", 6, 22, "", 6, 6, true, 3, true, 2, false, 3, false, 3, true, 2, false, 4, false, true);
        await this.ajoutePieceArmure(monActor43, "Chemise de mailles", 3, "Cuir épais");
        await this.ajouteArme(monActor43, "Morsure", "", 3, true, 6, 10, 14, "Perforation", "");
        await this.ajouteArme(monActor43, "Déchiquetage", "", 2, false, 6, 12, 12, "", "");
        await this.ajouteCapaciteSpeciale(monActor43, "Assaut brutal", "", "");
        await this.ajouteCapaciteSpeciale(monActor43, "Effroi", "", "");
        await this.ajouteCapaciteSpeciale(monActor43, "Peur du feu", "", "");
        await this.ajouteCapaciteSpeciale(monActor43, "Voix impérieuse", "", "");
        let monActor44 = await this.ajouteUnAdversaire(dossierLoups, "Dreorg le Wargelin", "modules/fvtt-tor1e-compendium/icons/adv-loup.webp", 5, 20, "Esprit d’Homme des Collines réfugié dans le corps d’un Warg, Dreorg est accompagné d’au moins 3 chefs de meute et les membres de sa meute n’ont pas peur du feu.", 8, 7, false, 3, false, 3, false, 2, false, 3, true, 1, false, 3, true, true);
        await this.ajoutePieceArmure(monActor44, "Chemise de mailles", 3, "Cuir épais");
        await this.ajouteArme(monActor44, "Morsure", "", 3, false, 5, 10, 18, "Perforation", "");
        await this.ajouteArme(monActor44, "Déchiquetage", "", 2, true, 5, 12, 14, "", "");
        await this.ajouteCapaciteSpeciale(monActor44, "Voix impérieuse", "", "");
        await this.ajouteCapaciteSpeciale(monActor44, "Assaut brutal", "", "");
        await this.ajouteCapaciteSpeciale(monActor44, "Grand bond", "", "");
        await this.ajouteCapaciteSpeciale(monActor44, "Effroi", "", "");
        let monActor45 = await this.ajouteUnAdversaire(dossierLoups, "Warg du Col du Rubicorne", "modules/fvtt-tor1e-compendium/icons/adv-loup.webp", 6, 20, "", 5, 6, false, 3, false, 3, true, 3, true, 2, true, 1, false, 3, false, false);
        await this.ajoutePieceArmure(monActor45, "Chemise de mailles", 3, "Cuir épais");
        await this.ajouteArme(monActor45, "Morsure", "", 3, true, 6, 10, 14, "Perforation", "");
        await this.ajouteArme(monActor45, "Déchiquetage", "", 1, true, 6, 12, 12, "", "");
        await this.ajouteCapaciteSpeciale(monActor45, "Effroi", "", "");
        await this.ajouteCapaciteSpeciale(monActor45, "Résistance abominable", "", "");
        await this.ajouteCapaciteSpeciale(monActor45, "Assaut brutal", "", "");
        await this.ajouteCapaciteSpeciale(monActor45, "Habitant des ténèbres", "", "");
        let monActor46 = await this.ajouteUnAdversaire(dossierLoups, "Chien-loup Sauvage", "modules/fvtt-tor1e-compendium/icons/adv-loup.webp", 2, 10, "", 2, 5, false, 2, false, 2, false, 3, false, 1, false, 0, false, 0, false, false);
        await this.ajoutePieceArmure(monActor46, "Corselet à manches longues", 2, "Cuir épais");
        await this.ajouteArme(monActor46, "Morsure", "", 2, false, 4, 10, 14, "Perforation", "");
        await this.ajouteCapaciteSpeciale(monActor46, "Etreinte", "", "");
        await this.ajouteCapaciteSpeciale(monActor46, "Grand bond", "", "");
        let monActor47 = await this.ajouteUnAdversaire(dossierVampires, "Grande chauve-souris", "modules/fvtt-tor1e-compendium/icons/adv-vampire.webp", 3, 10, "", 2, 5, false, 1, false, 3, false, 3, false, 3, false, 0, false, 0, true, false);
        await this.ajoutePieceArmure(monActor47, "Corselet à manches longues", 2, "Cuir épais");
        await this.ajouteArme(monActor47, "Morsure", "", 3, false, 3, 12, 16, "Perforation", "");
        await this.ajouteCapaciteSpeciale(monActor47, "Aversion au soleil", "", "");
        await this.ajouteCapaciteSpeciale(monActor47, "Etreinte", "", "");
        await this.ajouteCapaciteSpeciale(monActor47, "Habitant des ténèbres", "", "");
        await this.ajouteCapaciteSpeciale(monActor47, "Attaque plongeante", "", "");
        await this.ajouteCapaciteSpeciale(monActor47, "Déconcertant", "", "");
        let monActor48 = await this.ajouteUnAdversaire(dossierVampires, "Ombre secrète", "modules/fvtt-tor1e-compendium/icons/adv-vampire.webp", 5, 35, "", 6, 7, false, 2, false, 3, true, 3, true, 3, false, 2, false, 2, true, false);
        await this.ajoutePieceArmure(monActor48, "Chemise de mailles", 3, "Cuir très épais");
        await this.ajouteArme(monActor48, "Morsure", "", 3, true, 5, 12, 16, "Perforation", "");
        await this.ajouteArme(monActor48, "Balayage", "", 2, false, 5, 12, 14, "", "");
        await this.ajouteCapaciteSpeciale(monActor48, "Aversion au soleil", "", "");
        await this.ajouteCapaciteSpeciale(monActor48, "Vitesse du serpent", "", "");
        await this.ajouteCapaciteSpeciale(monActor48, "Habitant des ténèbres", "", "");
        await this.ajouteCapaciteSpeciale(monActor48, "Attaque plongeante", "", "");
        await this.ajouteCapaciteSpeciale(monActor48, "Déconcertant", "", "");
        await this.ajouteCapaciteSpeciale(monActor48, "Assaut brutal", "", "");
        let monActor49 = await this.ajouteUnAdversaire(dossierCreaturesDesMarais, "Hobgobelin", "modules/fvtt-tor1e-compendium/icons/adv-divers.webp", 4, 36, "", 5, 4, true, 1, false, 2, false, 3, true, 2, true, 1, false, 1, false, false);
        await this.ajoutePieceArmure(monActor49, "Corselet à manches longues", 2, "Cuir épais");
        await this.ajoutePieceArmure(monActor49, "Grand bouclier", 3, "Cuir très épais");
        await this.ajouteArme(monActor49, "Hache Orc", "", 3, true, 4, 12, 14, "Bouclier brisé", "");
        await this.ajouteArme(monActor49, "Morsure", "", 3, false, 5, 12, 16, "", "");
        await this.ajouteCapaciteSpeciale(monActor49, "Aversion au soleil", "", "");
        await this.ajouteCapaciteSpeciale(monActor49, "Habitant des ténèbres", "", "");
        await this.ajouteCapaciteSpeciale(monActor49, "Force effroyable", "", "");
        await this.ajouteCapaciteSpeciale(monActor49, "Pas de quartier", "", "");
        let monActor50 = await this.ajouteUnAdversaire(dossierCreaturesDesMarais, "Macrale", "modules/fvtt-tor1e-compendium/icons/adv-divers.webp", 5, 16, "", 4, 4, false, 2, false, 3, false, 2, true, 2, true, 2, false, 1, false, false);
        await this.ajoutePieceArmure(monActor50, "Corselet à manches longues", 2, "Cuir épais");
        await this.ajouteArme(monActor50, "Griffes", "", 3, true, 6, 12, 15, "", "");
        await this.ajouteCapaciteSpeciale(monActor50, "Etreinte", "", "");
        await this.ajouteCapaciteSpeciale(monActor50, "Vitesse du serpent", "", "");
        let monActor51 = await this.ajouteUnAdversaire(dossierCreaturesDesMarais, "Ogre des Marais", "modules/fvtt-tor1e-compendium/icons/adv-troll.webp", 6, 80, "", 6, 5, true, 1, false, 2, false, 2, true, 1, true, 0, false, 0, false, false);
        await this.ajoutePieceArmure(monActor51, "Corselet à manches longues", 2, "Cuir épais");
        await this.ajouteArme(monActor51, "Horion", "", 3, false, 6, 12, 12, "Renversement", "");
        await this.ajouteCapaciteSpeciale(monActor51, "Grande taille", "", "");
        await this.ajouteCapaciteSpeciale(monActor51, "Effroi", "", "");
        await this.ajouteCapaciteSpeciale(monActor51, "Acharnement", "", "");
        let monActor52 = await this.ajouteUnAdversaire(dossierCreaturesDesMarais, "Habitant des Marais", "modules/fvtt-tor1e-compendium/icons/adv-divers.webp", 4, 10, "", 2, 4, false, 2, false, 1, false, 1, false, 2, false, 1, true, 0, false, false);
        await this.ajoutePieceArmure(monActor52, "Chemise de mailles", 3, "Cuir très épais");
        await this.ajouteArme(monActor52, "Morsure", "", 2, false, 4, 12, 12, "", "");
        await this.ajouteArme(monActor52, "Griffes", "", 1, true, 5, 12, 16, "", "");
        await this.ajouteCapaciteSpeciale(monActor52, "Aversion au soleil", "", "");
        await this.ajouteCapaciteSpeciale(monActor52, "Puanteur fétide", "", "");
        let monActor53 = await this.ajouteUnAdversaire(dossierEsprits, "Esprit warg", "modules/fvtt-tor1e-compendium/icons/adv-esprit.webp", 3, 12, "Particularité : Fuit dans les ténèbres et disparait lorsque la haine est à zéro.", 3, 5, false, 1, false, 3, false, 2, true, 2, false, 0, false, 0, false, false);
        await this.ajouteArme(monActor53, "Morsure", "", 2, true, 3, 10, 14, "Perforation", "");
        await this.ajouteCapaciteSpeciale(monActor53, "Grand bond", "", "");
        await this.ajouteCapaciteSpeciale(monActor53, "Peur du feu", "", "");
        await this.ajoutePieceArmure(monActor53, "Corselet à manches longues", 2, "Forme d’esprit");
        let monActor54 = await this.ajouteUnAdversaire(dossierEsprits, "Spectre des bois", "modules/fvtt-tor1e-compendium/icons/adv-esprit.webp", 5, 54, "", 8, 7, false, 1, false, 3, false, 2, false, 1, false, 2, false, 3, false, false);
        await this.ajouteArme(monActor54, "Griffes étrangleuses", "", 3, false, 5, 12, 16, "", "");
        await this.ajouteCapaciteSpeciale(monActor54, "Habitant des ténèbres", "", "");
        await this.ajouteCapaciteSpeciale(monActor54, "Lâcheté", "", "");
        await this.ajouteCapaciteSpeciale(monActor54, "Peur du feu", "", "");
        await this.ajouteCapaciteSpeciale(monActor54, "Effroi", "Effroi (SR 16)", "");
        await this.ajouteCapaciteSpeciale(monActor54, "Horreur du Bois", "", "");
        await this.ajoutePieceArmure(monActor54, "Chemise de mailles", 4, "Forme d’esprit");
        let monActor55 = await this.ajouteUnAdversaire(dossierEsprits, "Le Roi au Gibet", "modules/fvtt-tor1e-compendium/icons/adv-esprit.webp", 8, 55, "Le Roi au Gibet n’attaque pas ses ennemis physiquement mais à chaque round, il prend un compagnon pour cible soit avec Epouvantables Sortilèges, soit avec Déconcertant, pour l’exposer aux attaques de ses esclaves.", 8, 5, true, 4, true, 1, false, 4, true, 1, false, 3, false, 2, false, true);
        await this.ajouteCapaciteSpeciale(monActor55, "Epouvantables sortilèges", "", "Envoutement : Test Corruption SR 14 raté = + 1 pt d’Ombre, héros figé sur place, incapable de se défendre ou d’attaquer pendant nb rounds = valeur actuelle d’Ombre, sauf si dépense de 1 pt d’Espoir / action.");
        await this.ajouteCapaciteSpeciale(monActor55, "Déconcertant", "", "");
        await this.ajouteCapaciteSpeciale(monActor55, "Effroi", "Souffrance d’autrui", "Le Roi au Gibet gagne 1 point de Haine à chaque fois qu’un compagnon est blessé ou a 0 en Endurance.");
        await this.ajouteCapaciteSpeciale(monActor55, "Abomination", "", "");
        await this.ajoutePieceArmure(monActor55, "Chemise de mailles", 3, "Forme d’esprit");
        let monActor56 = await this.ajouteUnAdversaire(dossierEsprits, "Raegenhere", "modules/fvtt-tor1e-compendium/icons/adv-esprit.webp", 8, 70, "", 10, 7, false, 3, false, 3, true, 3, false, 2, false, 4, true, 3, true, true);
        await this.ajouteArme(monActor56, "Grande hache", "", 3, true, 9, 12, 20, "Bouclier brisé", "");
        await this.ajouteArme(monActor56, "Griffes étrangleuses", "", 3, false, 5, 12, 16, "", "");
        await this.ajouteCapaciteSpeciale(monActor56, "Habitant des ténèbres", "", "");
        await this.ajouteCapaciteSpeciale(monActor56, "Assaut brutal", "", "");
        await this.ajouteCapaciteSpeciale(monActor56, "Résistance abominable", "", "");
        await this.ajouteCapaciteSpeciale(monActor56, "Peur du feu", "", "");
        await this.ajoutePieceArmure(monActor56, "Chemise de mailles", 3, "Forme d’esprit");
        let monActor57 = await this.ajouteUnAdversaire(dossierEsprits, "Fantôme de la Forêt", "modules/fvtt-tor1e-compendium/icons/adv-esprit.webp", 4, 50, "", 10, 6, false, 3, true, 3, false, 2, true, 3, false, 2, true, 2, false, true);
        await this.ajoutePieceArmure(monActor57, "Corselet à manches longues", 2, "Forme d’esprit");
        await this.ajouteArme(monActor57, "Epée longue (1m)", "", 3, false, 7, 9, 16, "Désarmer", "");
        await this.ajouteArme(monActor57, "Griffes", "", 2, true, 4, 12, 16, "", "");
        await this.ajouteCapaciteSpeciale(monActor57, "Attaque plongeante", "Attaque plongeante (forme éthérée)", "");
        await this.ajouteCapaciteSpeciale(monActor57, "Habitant des ténèbres", "", "");
        await this.ajouteCapaciteSpeciale(monActor57, "Abomination", "", "");
        await this.ajouteCapaciteSpeciale(monActor57, "Souffle Noir", "", "");
        await this.ajouteCapaciteSpeciale(monActor57, "Voix funeste", "", "");
        await this.ajouteCapaciteSpeciale(monActor57, "Epouvantables sortilèges", "", "Ombre de terreur : La cible fait un test de Présence SR10 + nivAttr. Echec : devient serviteur de l’Ombre pendant nivAttr semaines : obéit au spectre.");
        await this.ajouteCapaciteSpeciale(monActor57, "Peur du feu", "Peur du feu (forme tangible)", "");
        await this.ajouteCapaciteSpeciale(monActor57, "Dwimmerlaik", "Dwimmerlaik (forme tangible)", "");
        let monActor58 = await this.ajouteUnAdversaire(dossierEsprits, "Messager du Mordor", "modules/fvtt-tor1e-compendium/icons/adv-esprit.webp", 4, 50, "", 10, 6, false, 2, false, 3, true, 2, false, 3, true, 2, true, 2, false, true);
        await this.ajoutePieceArmure(monActor58, "Corselet à manches longues", 2, "Forme d’esprit");
        await this.ajouteArme(monActor58, "Epée longue (1m)", "", 2, true, 7, 9, 16, "Désarmer", "");
        await this.ajouteArme(monActor58, "Griffes", "", 3, false, 4, 12, 16, "", "");
        await this.ajouteCapaciteSpeciale(monActor58, "Déconcertant", "", "");
        await this.ajouteCapaciteSpeciale(monActor58, "Assaut brutal", "", "");
        await this.ajouteCapaciteSpeciale(monActor58, "Habitant des ténèbres", "", "");
        await this.ajouteCapaciteSpeciale(monActor58, "Abomination", "", "");
        await this.ajouteCapaciteSpeciale(monActor58, "Souffle Noir", "", "");
        await this.ajouteCapaciteSpeciale(monActor58, "Voix funeste", "", "");
        await this.ajouteCapaciteSpeciale(monActor58, "Epouvantables sortilèges", "", "Ombre de terreur : La cible fait un test de Présence SR10 + nivAttr. Echec : devient serviteur de l’Ombre pendant nivAttr semaines : obéit au spectre.");
        await this.ajouteCapaciteSpeciale(monActor58, "Peur du feu", "Peur du feu (forme tangible)", "");
        await this.ajouteCapaciteSpeciale(monActor58, "Dwimmerlaik", "Dwimmerlaik (forme tangible)", "");
        let monActor59 = await this.ajouteUnAdversaire(dossierEsprits, "Lieutenant de Dol Guldur", "modules/fvtt-tor1e-compendium/icons/adv-esprit.webp", 5, 60, "", 12, 7, true, 4, true, 3, false, 4, true, 2, false, 2, false, 4, true, true);
        await this.ajoutePieceArmure(monActor59, "Corselet à manches longues", 2, "Forme d’esprit");
        await this.ajouteArme(monActor59, "Epée longue (1m)", "", 3, true, 7, 9, 16, "Désarmer", "");
        await this.ajouteArme(monActor59, "Griffes", "", 3, false, 5, 12, 16, "", "");
        await this.ajouteCapaciteSpeciale(monActor59, "Force effroyable", "", "");
        await this.ajouteCapaciteSpeciale(monActor59, "Assaut brutal", "", "");
        await this.ajouteCapaciteSpeciale(monActor59, "Habitant des ténèbres", "", "");
        await this.ajouteCapaciteSpeciale(monActor59, "Abomination", "", "");
        await this.ajouteCapaciteSpeciale(monActor59, "Souffle Noir", "", "");
        await this.ajouteCapaciteSpeciale(monActor59, "Voix funeste", "", "");
        await this.ajouteCapaciteSpeciale(monActor59, "Epouvantables sortilèges", "", "Ombre de terreur : La cible fait un test de Présence SR10 + nivAttr. Echec : devient serviteur de l’Ombre pendant nivAttr semaines : obéit au spectre.");
        await this.ajouteCapaciteSpeciale(monActor59, "Peur du feu", "Peur du feu (forme tangible)", "");
        await this.ajouteCapaciteSpeciale(monActor59, "Dwimmerlaik", "Dwimmerlaik (forme tangible)", "");
        let monActor60 = await this.ajouteUnAdversaire(dossierEsprits, "Esprit de la nuit", "modules/fvtt-tor1e-compendium/icons/adv-esprit.webp", 4, 54, "Créature de l’Ombre qui hante la dépouille d’un guerrier mort. Elle attaque avec une lance à pointe barbelée et a recours à ses griffes si on la désarme.", 8, 7, false, 1, false, 3, true, 2, false, 1, false, 2, true, 3, false, false);
        await this.ajoutePieceArmure(monActor60, "Corselet à manches longues", 4, "Forme d’esprit");
        await this.ajouteArme(monActor60, "Lance", "", 3, true, 5, 9, 14, "Perforation", "");
        await this.ajouteArme(monActor60, "Griffes", "", 2, false, 4, 12, 16, "", "");
        await this.ajouteCapaciteSpeciale(monActor60, "Peur du feu", "", "");
        await this.ajouteCapaciteSpeciale(monActor60, "Attaque plongeante", "", "");
        await this.ajouteCapaciteSpeciale(monActor60, "Habitant des ténèbres", "", "");
        await this.ajouteCapaciteSpeciale(monActor60, "Lâcheté", "", "");
        let monActor61 = await this.ajouteUnAdversaire(dossierEsprits, "Guerrier mort-vivant", "modules/fvtt-tor1e-compendium/icons/adv-esprit.webp", 3, 15, "", 4, 3, false, 0, false, 2, true, 1, false, 1, true, 0, false, 0, false, false);
        await this.ajoutePieceArmure(monActor61, "Corselet à manches longues", 2, "Forme d’esprit");
        await this.ajouteArme(monActor61, "Griffes", "", 2, true, 3, 9, 12, "", "");
        await this.ajouteArme(monActor61, "Epée", "", 1, false, 5, 10, 16, "", "");
        await this.ajouteCapaciteSpeciale(monActor61, "Effroi", "", "");
        await this.ajouteCapaciteSpeciale(monActor61, "Vitalité surnaturelle de spectre", "", "");
        let monActor62 = await this.ajouteUnAdversaire(dossierEsprits, "Spectre d’archer hobbit", "modules/fvtt-tor1e-compendium/icons/adv-esprit.webp", 4, 30, "", 3, 5, false, 3, true, 4, true, 3, false, 1, false, 3, false, 2, false, false);
        await this.ajoutePieceArmure(monActor62, "Corselet à manches longues", 2, "Forme d’esprit");
        await this.ajouteCapaciteSpeciale(monActor62, "Effroi", "", "");
        await this.ajouteCapaciteSpeciale(monActor62, "Fantomatique", "", "");
        let monActor63 = await this.ajouteUnAdversaire(dossierEsprits, "Intendant de Carn Dûm", "modules/fvtt-tor1e-compendium/icons/adv-esprit.webp", 4, 70, "", 6, 5, false, 2, false, 4, false, 3, true, 0, false, 1, false, 3, false, true);
        await this.ajoutePieceArmure(monActor63, "Corselet à manches longues", 2, "Forme d’esprit");
        await this.ajoutePieceArmure(monActor63, "Bouclier", 2, "");
        await this.ajouteArme(monActor63, "Epée", "Lame rouillée", 2, true, 4, 10, 12, "Désarmer", "");
        await this.ajouteArme(monActor63, "Lance", "Lance impitoyable", 2, false, 4, 9, 12, "Perforation", "");
        await this.ajouteCapaciteSpeciale(monActor63, "Habitant des ténèbres", "", "");
        await this.ajouteCapaciteSpeciale(monActor63, "Peur du feu", "", "");
        await this.ajouteCapaciteSpeciale(monActor63, "Effroi", "", "");
        await this.ajouteCapaciteSpeciale(monActor63, "Vitalité surnaturelle de spectre", "", "");
        await this.ajouteCapaciteSpeciale(monActor63, "Spectral", "", "");
        let monActor64 = await this.ajouteUnAdversaire(dossierEsprits, "Le Roi des Esprits", "modules/fvtt-tor1e-compendium/icons/adv-esprit.webp", 6, 74, "", 12, 7, true, 5, true, 4, false, 3, false, 2, false, 4, false, 3, true, true);
        await this.ajoutePieceArmure(monActor64, "Corselet à manches longues", 2, "Forme d’esprit");
        await this.ajoutePieceArmure(monActor64, "Rondache", 1, "");
        await this.ajouteArme(monActor64, "Epée", "Epée ancestrale", 3, true, 6, 10, 14, "Désarmer", "");
        await this.ajouteArme(monActor64, "Griffes", "Toucher glacial", 3, false, 6, 12, 18, "Perforation", "");
        await this.ajouteCapaciteSpeciale(monActor64, "Aversion au soleil", "", "");
        await this.ajouteCapaciteSpeciale(monActor64, "Habitant des ténèbres", "", "");
        await this.ajouteCapaciteSpeciale(monActor64, "Abomination", "", "");
        await this.ajouteCapaciteSpeciale(monActor64, "Plus noir que les Ténèbres", "", "");
        await this.ajouteCapaciteSpeciale(monActor64, "Epouvantables sortilèges", "", "Complainte du Roi des Esprits : Le héros entend un air aux intonations abominables. Test de Corruption SR20, échec : isolé des compagnons et sommeil profond (inconscient). Héros trainé dans le tombeau pour y être sacrifié.");
        let monActor65 = await this.ajouteUnAdversaire(dossierEsprits, "Être des Galgals", "modules/fvtt-tor1e-compendium/icons/adv-esprit.webp", 4, 54, "", 6, 4, true, 2, true, 3, false, 2, true, 1, false, 1, false, 2, false, false);
        await this.ajoutePieceArmure(monActor65, "Corselet à manches longues", 2, "Forme d’esprit");
        await this.ajouteArme(monActor65, "Epée", "Epée ancestrale", 3, false, 6, 10, 14, "Désarmer", "La lame rouillée et acérée procure une sensation glaciale.");
        await this.ajouteArme(monActor65, "Griffes", "Toucher glacial", 1, true, 4, 12, 18, "Perforation", "Les mains osseuses se referment sur la chair de la victime qui reste figée.");
        await this.ajouteCapaciteSpeciale(monActor65, "Aversion au soleil", "", "");
        await this.ajouteCapaciteSpeciale(monActor65, "Habitant des ténèbres", "", "");
        await this.ajouteCapaciteSpeciale(monActor65, "Epouvantables sortilèges", "", "Complainte des Galgals : La créature fredonne un air qui fige le sang. Le héros fait un test de Corruption SR 16 : échec = isolé de ses compagnons et sombre dans un sommeil profond (considéré comme inconscient). Il est trainé jusqu’au tombeau où il sera sacrifié. Si retrouvé à temps : Guérison pour le réveiller.");
        await this.ajouteCapaciteSpeciale(monActor65, "Effroi", "", "");
        let monActor66 = await this.ajouteUnAdversaire(dossierEsprits, "Soldat fangeux", "modules/fvtt-tor1e-compendium/icons/adv-esprit.webp", 3, 12, "", 3, 3, false, 0, false, 2, false, 1, false, 1, false, 0, false, 0, false, false);
        await this.ajoutePieceArmure(monActor66, "Corselet à manches longues", 2, "Forme d’esprit");
        await this.ajouteArme(monActor66, "Griffes", "", 2, false, 3, 9, 12, "", "");
        await this.ajouteArme(monActor66, "Epée courte", "", 2, true, 5, 10, 14, "Désarmer", "");
        await this.ajouteCapaciteSpeciale(monActor66, "Effroi", "", "");
        await this.ajouteCapaciteSpeciale(monActor66, "Etreinte", "", "Si l’attaque principale de la créature a touché, la victime ne peut pas changer de position de combat et voit sa valeur de parade divisée par 2 (arrondie au supérieur). La créature qui étreint ne peut plus utiliser son attaque principale tant qu’elle retient sa cible mais peut utiliser son attaque secondaire si elle en a une. <br><br>En cas d’étreinte réussie, le soldat entraine la victime dans la boue. Chaque round : au lieu de combattre, le soldat peut faire un jet de Déplacement SR (10+Corps de la victime). Réussite : la victime est immergée et gravement gênée (défense avec SR-4, attaque avec SR+4)");
        await this.ajouteCapaciteSpeciale(monActor66, "Vitalité surnaturelle de spectre", "", "");
        let monActor67 = await this.ajouteUnAdversaire(dossierEsprits, "Spectre déchu", "modules/fvtt-tor1e-compendium/icons/adv-esprit.webp", 4, 35, "", 6, 5, false, 2, false, 4, false, 3, true, 0, false, 1, false, 3, false, false);
        await this.ajoutePieceArmure(monActor67, "Corselet à manches longues", 2, "Forme d’esprit");
        await this.ajoutePieceArmure(monActor67, "Bouclier", 2, "");
        await this.ajouteArme(monActor67, "Epée", "Lame rouillée", 2, true, 4, 10, 12, "Désarmer", "");
        await this.ajouteArme(monActor67, "Lance", "Lance impitoyable", 2, false, 4, 9, 12, "Perforation", "");
        await this.ajouteCapaciteSpeciale(monActor67, "Habitant des ténèbres", "", "");
        await this.ajouteCapaciteSpeciale(monActor67, "Peur du feu", "", "");
        await this.ajouteCapaciteSpeciale(monActor67, "Effroi", "", "");
        await this.ajouteCapaciteSpeciale(monActor67, "Vitalité surnaturelle de spectre", "", "");
        await this.ajouteCapaciteSpeciale(monActor67, "Spectral", "", "");
        let monActor68 = await this.ajouteUnAdversaire(dossierEsprits, "Apparition", "modules/fvtt-tor1e-compendium/icons/adv-esprit.webp", 4, 28, "Ames errantes pas forcément violentes, sèment désespoir et chagrin en partageant le leur. Elles se manifestent au cœur de la nuit près du lieu qu’elles hantent.", 6, 5, false, 3, true, 4, true, 3, false, 1, false, 3, false, 2, false, false);
        await this.ajoutePieceArmure(monActor68, "Corselet à manches longues", 2, "Forme d’esprit");
        await this.ajouteCapaciteSpeciale(monActor68, "Fantomatique", "", "");
        await this.ajouteCapaciteSpeciale(monActor68, "Effroi", "", "");
        await this.ajouteCapaciteSpeciale(monActor68, "Epouvantables sortilèges", "", "Accablement : Un héros qui rate son jet de Corruption SR 16 éprouve un chagrin insurmontable : +4* Pts d’Ombre. En cas d’Oeil de Sauron, une vieille plaie se rouvre et le héros devient Blessé.");
        await this.ajouteCapaciteSpeciale(monActor68, "Visions de Tourment", "", "");
        let monActor69 = await this.ajouteUnAdversaire(dossierEsprits, "Le Roi-Sorcier (forme intangible)", "modules/fvtt-tor1e-compendium/icons/adv-esprit.webp", 8, 90, "", 22, 0, false, 5, true, 3, true, 3, false, 3, true, 2, false, 5, true, true);
        await this.ajouteCapaciteSpeciale(monActor69, "Epouvantables sortilèges", "", "Interdiction : La cible est frappée de stupeur et perd sa prochaine action. Si Oeil de Sauron son arme se brise (ou tombe si arme magique). Ombre de Terreur : Lancé sur un Homme, Hobbit ou Nain pour l’asservir pendant 2 mois. Le héros s’efforce alors d’exécuter les instructions, par crainte et souvent à contrecœur, ce qui génère de l’angoisse. Ami : Jet de Présence SR 18.");
        await this.ajouteCapaciteSpeciale(monActor69, "Habitant des ténèbres", "", "");
        await this.ajouteCapaciteSpeciale(monActor69, "Abomination", "", "");
        await this.ajouteCapaciteSpeciale(monActor69, "Souffle Noir", "", "");
        await this.ajouteCapaciteSpeciale(monActor69, "Peur du feu", "", "");
        await this.ajouteCapaciteSpeciale(monActor69, "Voix funeste", "", "");
        await this.ajouteCapaciteSpeciale(monActor69, "Mot de Puissance et de Terreur", "", "");
        await this.ajouteCapaciteSpeciale(monActor69, "Peur Noire", "", "");
        let monActor70 = await this.ajouteUnAdversaire(dossierEsprits, "Le Roi-Sorcier (forme physique)", "modules/fvtt-tor1e-compendium/icons/adv-homme-mauvais.webp", 8, 90, "En combat, si Endurance=0, pas vaincu, Haine divisée par 2, + capacité Lâcheté. Ne peut être blessé que par des armes Fatales au Mordor. Disparait si Blessé et Endurance = 0.", 22, 12, false, 5, true, 3, true, 3, false, 3, true, 2, false, 5, true, true);
        await this.ajoutePieceArmure(monActor70, "Corselet à manches longues", 3, "Forme d’esprit");
        await this.ajouteArme(monActor70, "Epée longue (1m)", "", 4, true, 7, 9, 16, "Désarmer", "");
        await this.ajouteArme(monActor70, "Epée longue (2m)", "", 4, true, 9, 9, 18, "Désarmer", "");
        await this.ajouteArme(monActor70, "Griffes", "", 3, false, 8, 12, 16, "Perforation", "Les mains osseuses se referment sur la chair de la victime qui reste figée.");
        await this.ajouteArme(monActor70, "Poignard de Morgul", "", 3, true, 4, 12, 20, "Perforation", "Blessé par un Poignard de Morgul : voir page 84 de l’extension Fondcombe");
        await this.ajouteCapaciteSpeciale(monActor70, "Voix impérieuse", "", "");
        await this.ajouteCapaciteSpeciale(monActor70, "Souverain", "", "");
        await this.ajouteCapaciteSpeciale(monActor70, "Dwimmerlaik", "", "");
        await this.ajouteCapaciteSpeciale(monActor70, "Peur du feu", "", "");
        await this.ajouteCapaciteSpeciale(monActor70, "Force effroyable", "", "");
        await this.ajouteCapaciteSpeciale(monActor70, "Assaut brutal", "", "");
        let monActor71 = await this.ajouteUnAdversaire(dossierEsprits, "Húldrahir, spectre dúnadan", "modules/fvtt-tor1e-compendium/icons/adv-esprit.webp", 6, 35, "", 9, 7, false, 3, true, 4, true, 3, false, 1, false, 3, false, 2, false, true);
        await this.ajoutePieceArmure(monActor71, "Corselet à manches longues", 2, "Forme d’esprit");
        await this.ajouteCapaciteSpeciale(monActor71, "Fantomatique", "", "");
        await this.ajouteCapaciteSpeciale(monActor71, "Effroi", "", "");
        await this.ajouteCapaciteSpeciale(monActor71, "Epouvantables sortilèges", "", "Folie : Un héros qui rate son jet de Corruption SR 16 est vicitme de visions d’épouvante : +6 Pts d’Ombre. En cas d’Oeil de Sauron, le joueur perd le contrôle de son personnage pour un round de combat complet comme sous l’effet d’une crise de folie (Livre de base P 233).");
        await this.ajouteCapaciteSpeciale(monActor71, "Visions de Tourment", "", "");
        let monActor72 = await this.ajouteUnAdversaire(dossierDragonsBasilics, "Basilic", "modules/fvtt-tor1e-compendium/icons/adv-dragon.webp", 6, 40, "", 3, 4, false, 1, false, 2, false, 2, true, 3, false, 1, false, 1, false, false);
        await this.ajoutePieceArmure(monActor72, "Cotte de mailles", 4, "Ecailles");
        await this.ajouteArme(monActor72, "Morsure", "", 2, false, 4, 12, 14, "Perforation, Poison", "Poison du Basilic : Paralysé, la victime tombe au sol après min(Corps, Cœur) rounds. Il subit les effets de l’état Empoisonné. Après un jour, test de Vaillance SR 14. Succès : l’effet disparait, échec : paralysie permanente et mort en quelques mois.");
        await this.ajouteCapaciteSpeciale(monActor72, "Cuir robuste", "", "");
        await this.ajouteCapaciteSpeciale(monActor72, "Animosité (culture)", "Animosité (Elfes)", "");
        await this.ajouteCapaciteSpeciale(monActor72, "Résistance abominable", "", "");
        await this.ajouteCapaciteSpeciale(monActor72, "Souffle empoisonné", "", "");
        let monActor73 = await this.ajouteUnAdversaire(dossierDragonsBasilics, "Raenar", "modules/fvtt-tor1e-compendium/icons/adv-dragon.webp", 10, 120, "", 12, 12, true, 6, true, 5, true, 6, false, 3, false, 5, true, 4, true, true);
        await this.ajoutePieceArmure(monActor73, "Haubert de mailles", 6, "Ecailles de dragon");
        await this.ajouteArme(monActor73, "Morsure", "", 5, true, 10, 8, 18, "Perforation", "");
        await this.ajouteArme(monActor73, "Déchiquetage", "", 3, false, 7, 9, 20, "", "");
        await this.ajouteCapaciteSpeciale(monActor73, "Grande taille", "", "");
        await this.ajouteCapaciteSpeciale(monActor73, "Assaut brutal", "", "");
        await this.ajouteCapaciteSpeciale(monActor73, "Force effroyable", "", "");
        await this.ajouteCapaciteSpeciale(monActor73, "Abomination", "", "");
        await this.ajouteCapaciteSpeciale(monActor73, "Souffle empoisonné", "", "");
        await this.ajouteCapaciteSpeciale(monActor73, "Cuir robuste", "", "");
        await this.ajouteCapaciteSpeciale(monActor73, "Puanteur fétide", "", "");
        await this.ajouteCapaciteSpeciale(monActor73, "Epouvantables sortilèges", "", "Sortilège de dragon : Test de Corruption SR 16. Echec = +1 Pts d’Ombre + En combat la victime ne peut plus attaquer, En rencontre la victime est obligée de répondre sincèrement aux questions. Durée (10 – Sagesse) rounds.");
        await this.ajouteCapaciteSpeciale(monActor73, "Point faible", "", "Si Raenar fait un Coup Précis ou dépense 1 Pt de Haine, il expose son point faible à la prochaine attaque. Si touché avec coup perforant, test de Protection avec 1 seul dé, échec : rompt le combat et fuit.");
        let monActor74 = await this.ajouteUnAdversaire(dossierDragonsBasilics, "Le dragon de la Forêt", "modules/fvtt-tor1e-compendium/icons/adv-dragon.webp", 8, 99, "", 10, 6, true, 3, true, 4, true, 5, true, 4, true, 2, false, 3, false, true);
        await this.ajouteArme(monActor74, "Morsure", "", 3, true, 9, 9, 20, "Perforation", "");
        await this.ajouteArme(monActor74, "Déchiquetage", "Constriction", 3, false, 14, 12, 16, "", "La créature enroule sa queue autour de son ennemi pour l’écraser entre ses anneaux");
        await this.ajoutePieceArmure(monActor74, "Cotte de mailles", 4, "Ecailles de dragon");
        await this.ajouteCapaciteSpeciale(monActor74, "Grande taille", "", "");
        await this.ajouteCapaciteSpeciale(monActor74, "Cuir robuste", "", "");
        await this.ajouteCapaciteSpeciale(monActor74, "Force effroyable", "", "");
        await this.ajouteCapaciteSpeciale(monActor74, "Abomination", "", "");
        await this.ajouteCapaciteSpeciale(monActor74, "Habitant de la Forêt Noire", "", "");
        await this.ajouteCapaciteSpeciale(monActor74, "Epouvantables sortilèges", "", "Sortilège de dragon : Test de Corruption SR 14. Echec = +1 Pts d’Ombre + Paralysie permanente (échec) ou 4 rounds (succès) ou 1 round (succès supérieur). Rompre le sortilège : mort du Dragon ou soin d’un Magicien.");
        await this.ajouteCapaciteSpeciale(monActor74, "Point faible", "", "");
        let monActor75 = await this.ajouteUnAdversaire(dossierDivers, "Faucon macabre", "modules/fvtt-tor1e-compendium/icons/adv-divers.webp", 2, 10, "", 4, 5, false, 1, false, 2, false, 2, true, 2, false, 0, false, 0, false, false);
        await this.ajoutePieceArmure(monActor75, "Corselet à manches longues", 2, "Plumes épaisses");
        await this.ajouteArme(monActor75, "Morsure", "Bec", 2, true, 4, 10, 16, "Perforation", "");
        await this.ajouteArme(monActor75, "Griffes", "Serres", 1, false, 3, 12, 12, "", "");
        await this.ajouteCapaciteSpeciale(monActor75, "Assaut brutal", "", ""); 
        await this.ajouteCapaciteSpeciale(monActor75, "Vitesse du serpent", "", "");
        let monActor76 = await this.ajouteUnAdversaire(dossierDivers, "La chose du puits", "modules/fvtt-tor1e-compendium/icons/adv-divers.webp", 4, 45, "", 6, 4, true, 0, false, 2, true, 3, false, 0, false, 0, false, 2, false, true);
        await this.ajoutePieceArmure(monActor76, "Corselet à manches longues", 3, "Ecorce");
        await this.ajouteArme(monActor76, "Morsure", "Vrilles cinglantes", 3, true, 5, 12, 14, "", "Si attaque réussie : Utilise Etreinte pour immobiliser la cible avant d’attaquer par Etranglement.");
        await this.ajouteArme(monActor76, "Ecrasement", "Etranglement", 3, false, 6, 10, 14, "", "");
        await this.ajouteCapaciteSpeciale(monActor76, "Tentaculaire", "", "");
        await this.ajouteCapaciteSpeciale(monActor76, "Grande taille", "", "");
        await this.ajouteCapaciteSpeciale(monActor76, "Etreinte", "", "");
        let monActor77 = await this.ajouteUnAdversaire(dossierDivers, "L’épieur du Long Val", "modules/fvtt-tor1e-compendium/icons/adv-divers.webp", 9, 90, "", 9, 9, false, 2, false, 3, true, 2, false, 3, true, 0, false, 0, false, true);
        await this.ajoutePieceArmure(monActor77, "Haubert de mailles", 5, "Carapace");
        await this.ajouteArme(monActor77, "Ecrasement", "Pince écrasante", 3, true, 9, 8, 18, "Bouclier brisé", "");
        await this.ajouteArme(monActor77, "Griffes", "Coup de pattes", 2, false, 7, 10, 16, "Perforation", "");
        await this.ajouteCapaciteSpeciale(monActor77, "Insaisissable", "", "");
        await this.ajouteCapaciteSpeciale(monActor77, "Grande taille", "", "");
        await this.ajouteCapaciteSpeciale(monActor77, "Point faible", "", "");
        await this.ajouteCapaciteSpeciale(monActor77, "Force effroyable", "", "");
        await this.ajouteCapaciteSpeciale(monActor77, "Assaut brutal", "", "");
        let monActor78 = await this.ajouteUnAdversaire(dossierHommesMauvaisRhovanion, "Homme des Collines de Gundabad", "modules/fvtt-tor1e-compendium/icons/adv-homme-mauvais.webp", 4, 16, "", 3, 5, false, 1, false, 3, true, 2, false, 3, false, 2, false, 2, true, false);
        await this.ajoutePieceArmure(monActor78, "Corselet à manches longues", 2, "");
        await this.ajouteArme(monActor78, "Lance", "", 2, true, 5, 9, 14, "Perforation", "");
        await this.ajouteArme(monActor78, "Epée courte", "", 2, false, 5, 10, 14, "Désarmer", "");
        await this.ajouteCapaciteSpeciale(monActor78, "Effroi", "", "");
        await this.ajouteCapaciteSpeciale(monActor78, "Peur du feu", "", "");
        let monActor79 = await this.ajouteUnAdversaire(dossierHommesMauvaisRhovanion, "Homme Sauvage de la Forêt noire", "modules/fvtt-tor1e-compendium/icons/adv-homme-mauvais.webp", 3, 15, "", 3, 3, false, 2, false, 2, true, 2, false, 2, true, 1, false, 0, false, false);
        await this.ajoutePieceArmure(monActor79, "Chemise de mailles", 3, "");
        await this.ajouteArme(monActor79, "Lance", "", 2, true, 5, 9, 14, "Perforation", "");
        await this.ajouteArme(monActor79, "Arc", "", 2, false, 5, 10, 14, "Perforation", "");
        await this.ajouteCapaciteSpeciale(monActor79, "Animosité (culture)", "Animosité (Elfes)", "");
        await this.ajouteCapaciteSpeciale(monActor79, "Soumission (cible)", "Soumission (araignées)", "");
        await this.ajouteCapaciteSpeciale(monActor79, "Habitant de la Forêt Noire", "", "");
        let monActor80 = await this.ajouteUnAdversaire(dossierHommesMauvaisRhovanion, "Valdis (grand vampire)", "modules/fvtt-tor1e-compendium/icons/adv-vampire.webp", 7, 70, "", 12, 9, false, 3, false, 5, true, 4, true, 3, false, 2, false, 1, false, true);
        await this.ajoutePieceArmure(monActor80, "Chemise de mailles", 3, "Cuir épais");
        await this.ajouteArme(monActor80, "Balayage", "", 4, true, 7, 12, 16, "Perforation", "");
        await this.ajouteArme(monActor80, "Morsure", "", 4, false, 7, 12, 14, "", "");
        await this.ajouteCapaciteSpeciale(monActor80, "Aversion au soleil", "", "");
        await this.ajouteCapaciteSpeciale(monActor80, "Vitesse du serpent", "", "");
        await this.ajouteCapaciteSpeciale(monActor80, "Habitant des ténèbres", "", "");
        await this.ajouteCapaciteSpeciale(monActor80, "Attaque plongeante", "", "");
        await this.ajouteCapaciteSpeciale(monActor80, "Déconcertant", "", "");
        await this.ajouteCapaciteSpeciale(monActor80, "Assaut brutal", "", "");
        await this.ajouteCapaciteSpeciale(monActor80, "Grande taille", "", "");
        await this.ajouteCapaciteSpeciale(monActor80, "Envoûtement", "", "");
        let monActor81 = await this.ajouteUnAdversaire(dossierHommesMauvaisRhovanion, "Valter le Sanguinaire", "modules/fvtt-tor1e-compendium/icons/adv-homme-mauvais.webp", 5, 19, "Fils de Valind (chevalier et proche de Girion, Seigneur de Dale avant la venue de Smaug), Valter hérita de l’épée et de l’armure de son père, ce qui lui donna envie de fonder son propre royaume et de régner sur les hommes inférieurs. Chef-né, séduisant et éloquent, Valter a rencontré et pris la tête d’une bande de brigands. Pour lui, l’honneur et la vérité sont des illusions et seuls la force et le désir compte. Enfin Valter a le chic pour dénicher des secrets et trouver les faiblesses de caractère d’autrui. Rancune secrète, envie tenace, sentiment de frustration … : Valter s’en apercevra et vous promettra de tout faire pour y remédier … à condition que vous lui obéissiez.", 4, 5, false, 4, true, 3, true, 2, false, 2, false, 3, false, 2, false, true);
        await this.ajoutePieceArmure(monActor81, "Chemise de mailles", 3, "");
        await this.ajoutePieceArmure(monActor81, "Grand bouclier", 3, "");
        await this.ajoutePieceArmure(monActor81, "Heaume", 4, "");
        await this.ajouteArme(monActor81, "Epée", "", 3, false, 5, 10, 16, "Désarmer", "");
        await this.ajouteCapaciteSpeciale(monActor81, "Voix impérieuse", "", "");
        let monActor82 = await this.ajouteUnAdversaire(dossierHommesMauvaisRhovanion, "Archer hors-la-loi", "modules/fvtt-tor1e-compendium/icons/adv-homme-mauvais.webp", 3, 14, "", 1, 3, false, 2, false, 3, false, 2, true, 2, false, 1, false, 1, false, false);
        await this.ajoutePieceArmure(monActor82, "Corselet à manches longues", 2, "");
        await this.ajouteArme(monActor82, "Arc", "", 2, true, 5, 10, 14, "Perforation", "");
        await this.ajouteArme(monActor82, "Dague", "", 1, false, 3, 12, 12, "", "");
        await this.ajouteCapaciteSpeciale(monActor82, "Trait mortel", "", "");
        let monActor83 = await this.ajouteUnAdversaire(dossierHommesMauvaisRhovanion, "Guerrier hors-la-loi", "modules/fvtt-tor1e-compendium/icons/adv-homme-mauvais.webp", 3, 14, "", 2, 4, false, 2, false, 2, true, 2, false, 2, true, 1, false, 2, false, false);
        await this.ajoutePieceArmure(monActor83, "Chemise de mailles", 3, "");
        await this.ajoutePieceArmure(monActor83, "Rondache", 1, "");
        await this.ajouteArme(monActor83, "Lance", "", 2, true, 5, 9, 14, "Perforation", "");
        await this.ajouteArme(monActor83, "Hache", "", 2, false, 5, 12, 18, "Bouclier brisé", "");
        await this.ajouteCapaciteSpeciale(monActor83, "Pas de quartier", "", "");
        let monActor84 = await this.ajouteUnAdversaire(dossierHommesMauvaisRhovanion, "Guerrier oriental", "modules/fvtt-tor1e-compendium/icons/adv-homme-mauvais.webp", 3, 12, "", 2, 4, false, 2, false, 2, false, 1, false, 2, false, 2, true, 1, true, false);
        await this.ajoutePieceArmure(monActor84, "Corselet à manches longues", 2, "");
        await this.ajouteArme(monActor84, "Pique", "", 3, false, 7, 10, 15, "Bouclier brisé", "");
        await this.ajouteArme(monActor84, "Longue hache", "", 3, false, 7, 12, 18, "Bouclier brisé", "");
        await this.ajouteCapaciteSpeciale(monActor84, "Pas de quartier", "", "");
        let monActor85 = await this.ajouteUnAdversaire(dossierHommesMauvaisRhovanion, "Homme des Collines du Rhudaur", "modules/fvtt-tor1e-compendium/icons/adv-homme-mauvais.webp", 4, 18, "Bande disparate : Si des Gobelins accompagnent les Hommes des Collines, la Lâcheté est inopérante et le SR pour tendre une embuscade passe à 18.", 4, 5, false, 1, false, 3, true, 2, false, 3, false, 2, false, 2, true, false);
        await this.ajoutePieceArmure(monActor85, "Corselet à manches longues", 2, "");
        await this.ajouteArme(monActor85, "Hache Orc", "", 3, false, 5, 12, 16, "Bouclier brisé", "");
        await this.ajouteArme(monActor85, "Lance", "", 2, true, 5, 9, 14, "Perforation", "");
        await this.ajouteCapaciteSpeciale(monActor85, "Animosité (culture)", "Animosité (Dunedain)", "");
        await this.ajouteCapaciteSpeciale(monActor85, "Epouvantables sortilèges", "", "Malédiction des hommes : Le compagnon cible fait un Test de Corruption SR16. En cas d’échec, il gagne 1 point d’Ombre. Puis il perd 1 Point d’Espoir chaque fois qu’il obtient un Oeil de Sauron, jusqu’au prochain coucher ou lever du soleil.");
        await this.ajouteCapaciteSpeciale(monActor85, "Esprit corbeau", "", "");
        let monActor86 = await this.ajouteUnAdversaire(dossierHommesMauvaisRhovanion, "Heddwyn, serviteur-sorcier d’Angmar", "modules/fvtt-tor1e-compendium/icons/adv-homme-mauvais.webp", 6, 26, "Folie Ecarlate : La cible fait un Jet de Corruption. Echec : Le héros est Mélancolique jusqu’à la fin de la rencontre. En cas d’accès de folie (obtention d’un Oeil de Sauron, le héros cède à une rage incontrôlable et se retourne contre ses amis et alliés pendant un nb de rounds égal à la valeur de Sagesse. Le Gardien des Légendes prend le contrôle du héros pendant cette crise de folie.", 6, 6, true, 3, true, 2, false, 2, false, 3, false, 3, false, 3, true, true);
        await this.ajoutePieceArmure(monActor86, "Corselet à manches longues", 2, "");
        await this.ajouteArme(monActor86, "Lance", "", 3, true, 5, 9, 14, "Perforation", "");
        await this.ajouteArme(monActor86, "Epée courte", "", 2, false, 5, 10, 14, "Désarmer", "");
        await this.ajouteCapaciteSpeciale(monActor86, "Effroi", "", "");
        await this.ajouteCapaciteSpeciale(monActor86, "Haine débridée", "", "");
        await this.ajouteCapaciteSpeciale(monActor86, "Epouvantables sortilèges", "", "<p><strong>Givre et dégel</strong> : La cible fait un Jet de Corruption. Echec : Le héros subit les effets d’un froid extrême, il est épuisé pour le reste de la rencontre.</p><p><strong>Folie écarlate</strong> : a cible fait un Jet de Corruption. Echec : le héros visé est mélancolique pour le reste de la rencontre. En cas d'Oeil de Sauron il fera une crise de folie, cède à une rage incontrôlable et attaque ses amis et alliés</p>");
        let monActor87 = await this.ajouteUnAdversaire(dossierHommesMauvaisRhovanion, "Heddwyn, esprit-warg", "modules/fvtt-tor1e-compendium/icons/adv-homme-mauvais.webp", 5, 20, "S’il est blessé ou que sa Haine ou Endurance tombe à 0 : il bat en retraite vers son corps endormi dans ses quartiers. Les dégâts subis ne sont pas transférés à sa forme humaine (idem pour ses gardes du corps)", 5, 6, false, 2, false, 3, false, 2, false, 2, false, 0, false, 0, false, true);
        await this.ajoutePieceArmure(monActor87, "Corselet à manches longues", 2, "");
        await this.ajouteArme(monActor87, "Morsure", "", 3, true, 4, 10, 16, "Perforation", "");
        await this.ajouteCapaciteSpeciale(monActor87, "Grand bond", "", "");
        await this.ajouteCapaciteSpeciale(monActor87, "Peur du feu", "", "");
        let monActor88 = await this.ajouteUnAdversaire(dossierHommesMauvaisRhovanion, "Canaille d'Esgaroth", "modules/fvtt-tor1e-compendium/icons/adv-homme-mauvais.webp", 2, 12, "", 2, 4, false, 1, false, 2, false, 2, false, 2, false, 2, false, 1, false, false);
        await this.ajoutePieceArmure(monActor88, "Chemise de cuir", 1, "");
        await this.ajouteArme(monActor88, "Epée courte", "", 2, true, 5, 10, 14, "Désarmer", "");
        await this.ajouteCapaciteSpeciale(monActor88, "Lâcheté", "", "");
        let monActor89 = await this.ajouteUnAdversaire(dossierHommesMauvaisRhovanion, "Oderic", "modules/fvtt-tor1e-compendium/icons/adv-homme-mauvais.webp", 4, 18, "Originaire de Pierregué et adopté par Helmgut suite à la mort horrible de ses parents, Oderic est un jeune Béornide lunatique qui souhaite être aimé mais dont la mauvaise humeur et morosité lui aliènent toute sympathie. Excellent guerrier mais pas un meneur d’hommes, c’est son frère Rathfic qui fini par prendre la place d’Helmgut, ce qu’Oderic a vu comme une trahison. Ensuite, Brunhild a préféré épouser son frère alors qu’il en était amoureux. Souhaitant partir, son frère ivre l’a provoqué et la colère a mené au mauvais geste : il a tué son frère par accident. Arrêté puis débarrassé de ses geôliers par des Orcs, il rejoint une bande de hors-la-loi du Bois-aux-Loups.", 3, 5, false, 1, false, 2, true, 3, false, 2, false, 2, false, 1, false, true);
        await this.ajoutePieceArmure(monActor89, "Chemise de cuir", 1, "");
        await this.ajouteArme(monActor89, "Epée", "", 4, true, 5, 10, 16, "Désarmer", "");
        await this.ajouteArme(monActor89, "Hache", "", 2, false, 5, 12, 18, "Bouclier brisé", "");
        await this.ajouteCapaciteSpeciale(monActor89, "Assaut brutal", "", "");
        let monActor90 = await this.ajouteUnAdversaire(dossierHommesMauvaisRhovanion, "Guerrier de la Colline du Tyran", "modules/fvtt-tor1e-compendium/icons/adv-homme-mauvais.webp", 3, 16, "", 3, 4, false, 2, false, 2, false, 2, false, 3, false, 1, false, 3, false, false);
        await this.ajoutePieceArmure(monActor90, "Corselet à manches longues", 2, "");
        await this.ajoutePieceArmure(monActor90, "Bouclier", 1, "");
        await this.ajouteArme(monActor90, "Epée", "", 3, false, 5, 12, 16, "Désarmer", "");
        await this.ajouteArme(monActor90, "Morsure", "Molosse", 3, false, 4, 12, 14, "", "");
        await this.ajouteCapaciteSpeciale(monActor90, "Dresseur", "", "");
        await this.ajouteCapaciteSpeciale(monActor90, "Pas de quartier", "", "");
        let monActor91 = await this.ajouteUnAdversaire(dossierHommesMauvaisEriador, "Thark, chef des Hommes des Collines", "modules/fvtt-tor1e-compendium/icons/adv-homme-mauvais.webp", 6, 24, "Thark et ses fidèles offrent leur service (payant) aux voyageurs en tant que guide et éclaireur. Si refus, Thark exige un droit de passage. Dans tous les cas, il guide les voyageurs vers un lieu où il disposera d’un avantage.", 4, 5, false, 3, false, 3, true, 2, false, 3, false, 2, false, 2, true, true);
        await this.ajoutePieceArmure(monActor91, "Corselet à manches longues", 2, "");
        await this.ajouteArme(monActor91, "Hache Orc", "", 3, false, 5, 12, 16, "Bouclier brisé", "");
        await this.ajouteArme(monActor91, "Lance", "", 2, true, 5, 9, 14, "Perforation", "");
        await this.ajouteCapaciteSpeciale(monActor91, "Animosité (culture)", "animosité (Dûnedains)", "");
        await this.ajouteCapaciteSpeciale(monActor91, "Epouvantables sortilèges", "", "Malédiction des Hommes des Collines : La cible fait un Test de Corruption SR16. En cas d’échec, il gagne 1 point d’Ombre. Puis il perd 1 Point d’Espoir chaque fois qu’il obtient un Oeil de Sauron, jusqu’au prochain coucher ou lever du soleil.");
        await this.ajouteCapaciteSpeciale(monActor91, "Esprit corbeau", "", "");
        await this.ajouteCapaciteSpeciale(monActor91, "Connaissances régionales (région)", "Connaissances Régionales (Angmar) ", "");
        await this.ajouteCapaciteSpeciale(monActor91, "Montagnard", "", "");
        let monActor92 = await this.ajouteUnAdversaire(dossierHommesMauvaisEriador, "Bandits impitoyables", "modules/fvtt-tor1e-compendium/icons/adv-homme-mauvais.webp", 4, 14, "", 3, 4, false, 1, false, 2, true, 2, false, 2, true, 1, false, 2, false, false);
        await this.ajoutePieceArmure(monActor92, "Chemise de cuir", 1, "");
        await this.ajoutePieceArmure(monActor92, "Rondache", 1, "");
        await this.ajouteArme(monActor92, "Epée", "", 2, true, 5, 10, 16, "Désarmer", "");
        await this.ajouteArme(monActor92, "Arc", "", 1, false, 5, 10, 14, "Perforation", "");
        await this.ajouteCapaciteSpeciale(monActor92, "Pas de quartier", "", "");
        let monActor93 = await this.ajouteUnAdversaire(dossierHommesMauvaisRhovanion, "Beorn (forme d’Ours)", "modules/fvtt-tor1e-compendium/icons/adv-homme-mauvais.webp", 9, 99, "", 12, 12, false, 5, true, 4, true, 3, false, 3, false, 0, false, 3, false, );
        await this.ajoutePieceArmure(monActor93, "Cotte de mailles", 4, "Cuir épais");
        await this.ajouteArme(monActor93, "Lacération", "", 4, true, 9, 10, 16, "Bouclier brisé", "");
        await this.ajouteCapaciteSpeciale(monActor93, "Abomination", "Gigantesque", "Si Beorn est Blessé 1 fois ou si son Endurance est réduite à 0 : Il continue de se battre jusqu’à une nouvelle Blessure ou perte d’Endurance (en étant déjà blessé)");
        await this.ajouteCapaciteSpeciale(monActor93, "Acharnement", "Coriace", "Réduire de 9 points la perte d’Endurance causée par une attaque adverse");
        await this.ajouteCapaciteSpeciale(monActor93, "Acharnement", "Force dévastatrice", "Après une attaque réussie, les points d’Endurance perdus par la cible sont augmenté de 9 points.");
        await this.ajouteCapaciteSpeciale(monActor93, "Acharnement", "Courroux meurtrier", "Jet de Personnalité SR 10 + max Niv attribut des adversaires. Succès : L’ennemi perd des Points de Haine: 1 Pt + 1 pt par 6 (répartis par le Gardien des Légendes)");
        let monActor94 = await this.ajouteUnAdversaire(dossierHommesMauvaisRhovanion, "Gerold le beornide", "modules/fvtt-tor1e-compendium/icons/adv-homme-mauvais.webp", 7, 20, "", 4, 4, false, 2, false, 4, true, 3, false, 2, true, 2, false, 1, false, );
        await this.ajoutePieceArmure(monActor94, "Chemise de mailles", 3, "");
        await this.ajouteArme(monActor94, "Hache", "", 4, true, 5, 12, 18, "Bouclier brisé", "");
        await this.ajouteCapaciteSpeciale(monActor94, "Pas de quartier", "Force dévastatrice", "Après une attaque réussie, les points d’Endurance perdus par la cible sont augmenté de 7 points.");
        let monActor95 = await this.ajouteUnAdversaire(dossierHommesMauvaisEriador, "Nain voleur", "modules/fvtt-tor1e-compendium/icons/adv-homme-mauvais.webp", 4, 18, "<p>Si les Nains sont généralement travailleurs, ronchons mais honnêtes, certains sont hélas coulés dans un autre moule : Trouvant que travailler pour un profit incertain était une perte de temps, certains nains préfèrent utiliser des méthodes plus directes pour faire fortune.</p>", 3, 4, false, 0, false, 2, true, 2, true, 1, false, 0, false, 3, false, );
        await this.ajoutePieceArmure(monActor95, "Chemise de mailles", 3, "");
        await this.ajouteArme(monActor95, "Bigot", "", 2, true, 8, 10, 18, "Bouclier brisé", "");
        let monActor96 = await this.ajouteUnAdversaire(dossierHommesMauvaisEriador, "Lófar l’aigrefin", "modules/fvtt-tor1e-compendium/icons/adv-homme-mauvais.webp", 6, 22, "<p>Originaire des Montagnes Bleues, Lófar y souffre d’une réputation exécrable en raison de sa propension au larcin qui lui a valu d’en être banni.</p><p>Mineur de formation, il s’est reconverti dans le pillage de tombes. Il est assez malin pour éviter les Hauts des Galgals et il s’est établi dans les ruines de Fornost.</p><p>A la tête d’une petite bande de nains, ils œuvrent en sous-sol pour ne pas être repérés par les Rôdeurs. S’ils ont mis la main sur de nombreux trésors, Lófar et ses comparses sont maintenant pris par le Mal du Dragon.</p>", 5, 6, false, 3, true, 2, true, 3, true, 1, false, 1, false, 3, false, );
        await this.ajouteCapaciteSpeciale(monActor96, "Voix impérieuse", "", "");
        await this.ajouteArme(monActor96, "Bigot", "", 4, true, 8, 10, 18, "Bouclier brisé", "");
        let monActor97 = await this.ajouteUnAdversaire(dossierHommesMauvaisEriador, "Sergent Cyrnan", "modules/fvtt-tor1e-compendium/icons/adv-homme-mauvais.webp", 5, 22, "", 5, 5, true, 3, true, 3, true, 2, false, 3, false, 2, false, 2, false, );
        await this.ajoutePieceArmure(monActor97, "Corselet à manches longues", 2, "");
        await this.ajoutePieceArmure(monActor97, "Rondache", 1, "");
        await this.ajouteArme(monActor97, "Epée", "", 3, true, 5, 10, 16, "Désarmer", "");
        await this.ajouteCapaciteSpeciale(monActor97, "Voix impérieuse", "", "");
        await this.ajouteCapaciteSpeciale(monActor97, "Pas de quartier", "", "");
        let monActor98 = await this.ajouteUnAdversaire(dossierHommesMauvaisEriador, "Caradog, chasseur du Pays de Den", "modules/fvtt-tor1e-compendium/icons/adv-homme-mauvais.webp", 5, 15, "", 4, 4, false, 1, false, 2, true, 3, false, 3, false, 1, false, 3, false, );
        await this.ajoutePieceArmure(monActor98, "Corselet à manches longues", 2, "");
        await this.ajouteArme(monActor98, "Lance", "", 2, true, 5, 6, 14, "Perforation", "");
        await this.ajouteArme(monActor98, "Dague", "", 2, false, 3, 12, 12, "", "");
        await this.ajouteCapaciteSpeciale(monActor98, "Pas de quartier", "", "");
        let monActor99 = await this.ajouteUnAdversaire(dossierHommesMauvaisEriador, "Wathach la voyante", "modules/fvtt-tor1e-compendium/icons/adv-homme-mauvais.webp", 6, 14, "", 8, 4, false, 3, true, 2, false, 3, false, 1, false, 3, false, 3, true, );
        await this.ajoutePieceArmure(monActor99, "Corselet à manches longues", 2, "");
        await this.ajouteArme(monActor99, "Couteau à dents", "", 3, true, 3, 12, 12, "", "");
        await this.ajouteCapaciteSpeciale(monActor99, "Voix impérieuse", "", "");
        let monActor100 = await this.ajouteUnAdversaire(dossierHommesMauvaisEriador, "Ruffians", "modules/fvtt-tor1e-compendium/icons/adv-homme-mauvais.webp", 2, 8, "", 1, 3, false, 2, false, 1, false, 2, true, 1, false, 1, false, 1, true, );
        await this.ajoutePieceArmure(monActor100, "Chemise de cuir", 1, "");
        await this.ajouteArme(monActor100, "Dague", "", 3, true, 3, 12, 12, "", "");
        await this.ajouteCapaciteSpeciale(monActor100, "Lâcheté", "", "");
        let monActor101 = await this.ajouteUnAdversaire(dossierHommesMauvaisEriador, "Homme d’arme de la Compagnie du Chariot", "modules/fvtt-tor1e-compendium/icons/adv-homme-mauvais.webp", 4, 12, "", 2, 3, false, 1, false, 2, true, 2, false, 2, false, 1, false, 2, true, );
        await this.ajoutePieceArmure(monActor101, "Chemise de cuir", 1, "");
        await this.ajouteArme(monActor101, "Lance", "", 2, true, 5, 9, 14, "Perforation", "");
        await this.ajouteArme(monActor101, "Dague", "", 2, true, 5, 10, 14, "Perforation", "");
        await this.ajouteArme(monActor101, "Arc", "", 1, false, 3, 12, 12, "", "");
        let monActor102 = await this.ajouteUnAdversaire(dossierHommesMauvaisEriador, "Guerrier de Dun", "modules/fvtt-tor1e-compendium/icons/adv-homme-mauvais.webp", 5, 16, "", 4, 5, false, 2, true, 3, false, 2, true, 3, false, 1, false, 1, false, );
        await this.ajoutePieceArmure(monActor102, "Corselet à manches longues", 2, "");
        await this.ajouteArme(monActor102, "Hache à long manche (1m)", "", 3, true, 5, 12, 18, "Bouclier brisé", "");
        await this.ajouteArme(monActor102, "Grande lance", "", 3, true, 9, 9, 16, "Perforation", "");
        await this.ajouteArme(monActor102, "Dague", "", 1, false, 3, 12, 12, "", "");
        await this.ajouteCapaciteSpeciale(monActor102, "Pas de quartier", "", "");
        let monActor103 = await this.ajouteUnAdversaire(dossierHommesMauvaisEriador, "Villageois armé", "modules/fvtt-tor1e-compendium/icons/adv-homme-mauvais.webp", 3, 10, "", 1, 3, false, 1, false, 1, false, 2, false, 2, false, 1, true, 1, false, );
        await this.ajoutePieceArmure(monActor103, "Chemise de cuir", 1, "");
        await this.ajouteArme(monActor103, "Epée", "", 2, false, 5, 10, 16, "Désarmer", "");
        await this.ajouteArme(monActor103, "Lance", "", 1, true, 5, 9, 14, "Perforation", "");
        await this.ajouteArme(monActor103, "Arc", "", 3, false, 5, 10, 14, "Perforation", "");
        await this.ajouteArme(monActor103, "Dague", "", 1, false, 3, 12, 12, "", "");
        let monActor104 = await this.ajouteUnAdversaire(dossierRohan, "Thelred, cavalier renégat du Rohan", "modules/fvtt-tor1e-compendium/icons/adv-homme-mauvais.webp", 6, 15, "", 5, 6, true, 2, false, 3, true, 3, true, 2, false, 1, false, 3, false, );
        await this.ajoutePieceArmure(monActor104, "Corselet à manches longues", 2, "");
        await this.ajouteArme(monActor104, "Epée", "", 3, false, 5, 10, 16, "Désarmer", "");
        await this.ajouteArme(monActor104, "Lance", "", 3, true, 5, 9, 14, "Perforation, Poison*", "*Poison : La lance de Thelred est enduite d’un poison concocté par Ainfean, cadeau d’Uathach. Un personnage touché par la lance s’écroule, pris de convulsions, et se met à écumer au bout d’un nombre de rounds égal à sa valeur de Corps (en plus des effets classiques de l’effet Empoisonné).");
        await this.ajouteArme(monActor104, "Dague", "", 2, false, 3, 12, 12, "", "");
        await this.ajouteCapaciteSpeciale(monActor104, "Pas de quartier", "", "");
        let monActor105 = await this.ajouteUnAdversaire(dossierOrcsGob, "Rugash le Serpent, chef Orc", "modules/fvtt-tor1e-compendium/icons/adv-orc.webp", 5, 28, "<p>Rugash n’est pas un chef orque comme les autres, ce qui explique en grande partie pourquoi sa tribu a tant pros- péré dans le Désert. À l’époque où il rejoignit la Bataille des Cinq Armées, il n’était ni le plus fort ni le plus talentueux de ses congénères. Mais il était rusé et, chose rare pour un Orque, subtil. Il savait quoi dire auprès de tel ou tel commandant afin de diriger les évènements de la manière dont il le souhaitait. Pendant de longues années, il se contenta de rester à l’arrière-plan, laissant les autres subir les défis et les trahisons qu’affrontent régulièrement la plupart des chefs orques. Ce ne fut pas la défaite de la Bataille des Cinq Armées qui changea le destin de Rugash, mais ses conséquences.</p><p>Quand il sentit la bataille tourner à l’avantage de ses ennemis, Rugash s’enfuit vers le Nord avant la déroute. Il avait déjà planifié sa fuite et trouvé un ravin bien dis- simulé où se cacher. Grâce à cette clairvoyance, il évita le sort de ses camarades, tués par les Aigles pendant leur fuite. Tandis que les survivants orques et gobelins se rassemblaient, Rugash se mêla à un groupe mené par un rustre bien décidé à se venger rapidement des Nains. Rugash l’empoisonna discrètement et prit sa place.</p><p>La stratégie de Rugash s’est révélée payante dans le Désert, où la ruse compte généralement plus que la force. Aussi insaisissable qu’une ombre, sa tribu déplace souvent son camp et a tendance à frapper à la faveur d’une tempête de sable. Ce sont ses guerriers qui l’ont surnommé « le Serpent », un sobriquet qu’il porte avec fierté. Après cette longue série de succès, les ambitions de Rugash ont évo- lué. À présent, ses explorateurs observent minutieusement des cibles de choix le long de la frontière orientale de la région de Dale en prévision de prochains raids.</p>", 7, 5, false, 4, true, 3, true, 2, false, 3, true, 1, false, 2, false, true);
        await this.ajoutePieceArmure(monActor105, "Chemise de mailles", 3, "");
        await this.ajoutePieceArmure(monActor105, "Grand bouclier", 3, "");
        await this.ajouteArme(monActor105, "Hache", "Hache Orc", 2, false, 5, 12, 16, "Bouclier brisé", "");
        await this.ajouteArme(monActor105, "Lance", "", 4, true, 7, 10, 12, "Poison", "Le Croc du Serpent est une lance à tête large et fendue");
        await this.ajouteCapaciteSpeciale(monActor105, "Aversion au soleil", "", "");
        await this.ajouteCapaciteSpeciale(monActor105, "Vitesse du serpent", "", "");
        await this.ajouteCapaciteSpeciale(monActor105, "Voix impérieuse", "", "");
        
        console.log("===== FIN CREATION DES ADVERSAIRES ====");
    }

    async ajouteUnAdversaire(_dossier, _nom, _img, _attLevel, _endurance, _description, _hate, _parry, _armureFav, _skillPersoRang, _skillPersoFav, _skillMoveRang, _skillMoveFav, _skillPerceptionRang, _skillPerceptionFav, _skillSurvieRang, _skillSurvieFav, _skillCustomRang, _skillCustomFav, _skillVocationRang, _skillVocationFav, _tokenLie) {
        console.log("===> ajout de " + _nom);
        let monActor = await Actor.create({ name: _nom, type: "adversary", folder: _dossier._id, img: _img, data: { stateOfHealth: { weary: { value: false } }, attributeLevel: { value: _attLevel }, endurance: { value: _endurance, max: _endurance }, description: { value: "<p>" + _description + "</p>" }, hate: { value: _hate, max: _hate }, parry: { value: _parry }, armour: { favoured: { value: _armureFav } }, skills: { personality: { value: _skillPersoRang, favoured: { value: _skillPersoFav } }, movement: { value: _skillMoveRang, favoured: { value: _skillMoveFav } }, perception: { value: _skillPerceptionRang, favoured: { value: _skillPerceptionFav } }, survival: { value: _skillSurvieRang, favoured: { value: _skillSurvieFav } }, custom: { value: _skillCustomRang, favoured: { value: _skillCustomFav } }, vocation: { value: _skillVocationRang, favoured: { value: _skillVocationFav } } } } });

        const update = {
        };
        update.token = {
            actorLink: _tokenLie,
            disposition: CONST.TOKEN_DISPOSITIONS.HOSTILE,
            bar1: {
                attribute: "endurance"
            },
            bar2: {
                attribute: "hate"
            },
            displayBars: CONST.TOKEN_DISPLAY_MODES.OWNER_HOVER,
            displayName: CONST.TOKEN_DISPLAY_MODES.OWNER_HOVER,
            vision: false
        };
        await monActor.update(update);
        
        return monActor;
    }
        
    async ajouteCapaciteSpeciale(monActor, nomCapacite, nouveauNom, descriptionAdditionnelle) {
        // Chercher la capacité spéciale dans les Items
        const maCapacite = game.items.contents.find(i => i.type === "special-ability" && i.name === nomCapacite);

        // Créer une copie de l'item dans la feuille de personnage
        await monActor.createEmbeddedDocuments('Item', [maCapacite], { renderSheet: false });


        if (nouveauNom !== "" || descriptionAdditionnelle !== "") {
            // récupérer l'item dans la feuille de perso
            const monItem = monActor.items.find(i => i.type === "special-ability" && i.name === nomCapacite);

            // Construire la mise à jour des données
            let newDescription = "";
            if (descriptionAdditionnelle !== "") {
                if (monItem.system.description.value !== null) {
                    newDescription = "<p>" + descriptionAdditionnelle + "</p>" + monItem.system.description.value;
                } else {
                    newDescription = "<p>" + descriptionAdditionnelle + "</p>";
                }
            } else {
                newDescription = monItem.system.description.value;
            }

            let newNom = "";
            if (nouveauNom !== "") {
                newNom = nouveauNom;
            } else {
                newNom = monItem.name;
            }

            let monUpdate = {
                _id: monItem._id,
                name: newNom,
                data: {
                    description: {
                        value: newDescription
                    }
                }
            };

            // Appliquer la mise à jour
            const monUpdated = await monActor.updateEmbeddedDocuments('Item',[monUpdate]);
        }
    }

    async ajoutePieceArmure(monActor, nomArmure, protection, nouveauNom) {
        // Chercher l'armure dans les Items
        const monArmure = game.items.contents.find(i => i.type === "armour" && i.name === nomArmure);

        // Créer une copie de l'item dans la feuille de personnage
        await monActor.createEmbeddedDocuments('Item',[monArmure], { renderSheet: false });

        // récupérer l'item dans la feuille de perso
        const monItem = monActor.items.find(i => i.type === "armour" && i.name === nomArmure);

        // Construire la mise à jour des données
        let monUpdate = "";
        if (nouveauNom !== "") {
            monUpdate = {
                _id: monItem._id,
                name: nouveauNom,
                data: {
                    protection: {
                        value: protection
                    },
                    equipped: {
                        value: true
                    }
                }
            };
        } else {
            monUpdate = {
                _id: monItem._id,
                data: {
                    protection: {
                        value: protection
                    },
                    equipped: {
                        value: true
                    }
                }
            };
        }

        // Appliquer la mise à jour
        const monUpdated = await monActor.updateEmbeddedDocuments('Item',[monUpdate]);
    }

    async ajouteArme(monActor, nomArme, nouveauNom, rang, fav, degats, taille, blessure, coupPrecis, descriptionAdditionnelle) {
        // Chercher l'arme dans les Items
        const monArme = game.items.contents.find(i => i.type === "weapon" && i.name === nomArme);

        // Créer une copie de l'item dans la feuille de personnage
        await monActor.createEmbeddedDocuments('Item',[monArme], { renderSheet: false });

        // récupérer l'item dans la feuille de perso
        const monItem = monActor.items.find(i => i.type === "weapon" && i.name === nomArme);

        // Revoir la description si besoin
        let newDescription = "";
        if (descriptionAdditionnelle !== "") {
            if (monItem.system.description.value !== null) {
                newDescription = "<p>" + descriptionAdditionnelle + "</p>" + monItem.system.description.value;
            } else {
                newDescription = "<p>" + descriptionAdditionnelle + "</p>";
            }
        } else {
            newDescription = monItem.system.description.value;
        }

        // Déterminer le nom final
        let newNom = nomArme;
        if (nouveauNom !== "") {
            newNom = nouveauNom;
        }

        // Construire la mise à jour des données
        const monUpdate = {
            _id: monItem._id,
            name: newNom,
            data: {
                favoured: {
                    value: fav
                },
                skill: {
                    value: rang
                },
                damage: {
                    value: degats
                },
                edge: {
                    value: taille
                },
                injury: {
                    value: blessure
                },
                calledShot: {
                    value: coupPrecis
                },
                description: {
                    value: newDescription
                }
            }
        };

        // Appliquer la mise à jour
        const monUpdated = await monActor.updateEmbeddedDocuments('Item',[monUpdate]);
    }
}