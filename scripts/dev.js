export class Tor1eCompendiumDev {

    async dev_Delete_all() {
        new Dialog({
            title: 'Suppression des Actors, Items ou Folders',
            content: `
              <form>
                <div class="form-group">
                  <label>Type d'objet à supprimer</label>
                  <select id="objet-Type" />
                    <option value="actors">Actor</option>
                    <option value="items">Item</option>
                    <option value="folders">Folder</option>
                  </select>
                  <label>Uniquement les objets à la racine ?</label>
                  <select id="objet-racine" />s
                    <option value="oui">Oui</option>
                    <option value="non">Non</option>
                  </select>
                </div>
              </form>`,
            buttons: {
                yes: {
                    icon: "<i class='fas fa-check'></i>",
                    label: `Continuer`
                }
            },
            default: 'yes',
            close: html => {

                let objetType = html.find('select#objet-Type')[0]?.value || null;
                let objetRacine = html.find('select#objet-racine')[0]?.value || null;

                if (objetType !== null && objetRacine != null) {
                    console.log("Suppression des objets de type " + objetType);
                    if (objetRacine == "oui")
                        console.log("   et situés à la racine");
                    else
                        console.log("   et situés à tous les niveaux");

                    if (objetType == "items") {
                        game.items.forEach(t => {
                            if (objetRacine == "oui") {
                                if (!t.folder) {
                                    t.delete();
                                }
                            } else {
                                t.delete();
                            }
                        });
                    }

                    if (objetType == "actors") {
                        game.actors.forEach(t => {
                            if (objetRacine == "oui") {
                                if (!t.folder) {
                                    t.delete();
                                }
                            } else {
                                t.delete();
                            }
                        });
                    }

                    if (objetType == "folders") {
                        game.folders.forEach(t => {
                            if (objetRacine == "oui") {
                                if (!t.folder) {
                                    t.delete();
                                }
                            } else {
                                t.delete();
                            }
                        });
                    }
                }
            }
        }).render(true);
    }
    async dev_ExportActor_to_Json() {
        new Dialog({
            title: 'Export Actor to JSON',
            content: `
              <form>
                <div class="form-group">
                  <label>Indiquer le nom de l'Actor</label>
                  <input type='text' name='objet-Nom'></input>
                </div>
              </form>`,
            buttons: {
                yes: {
                    icon: "<i class='fas fa-check'></i>",
                    label: `Continuer`
                }
            },
            default: 'yes',
            close: html => {

                let objetNom = html.find('input[name=\'objet-Nom\']').val();

                if (objetNom !== '') {
                    game.actors.forEach(t => {
                        if (t.name == objetNom) {
                            console.log(t);
                            t.exportToJSON();
                        }
                    });
                }
            }
        }).render(true);
    }

    async dev_Debug() {
        new Dialog({
            title: 'Debug Actor, Item or Folder',
            content: `
              <form>
                <div class="form-group">
                  <label>Indiquer le nom et le type de l'objet</label>
                  <select id="objet-Type" />
                    <option value="actors">Actor</option>
                    <option value="items">Item</option>
                    <option value="folders">Folder</option>
                  </select>
                  <input type='text' name='objet-Nom'></input>
                </div>
              </form>`,
            buttons: {
                yes: {
                    icon: "<i class='fas fa-check'></i>",
                    label: `Continuer`
                }
            },
            default: 'yes',
            close: html => {

                let objetType = html.find('select#objet-Type')[0]?.value || null;
                let objetNom = html.find('input[name=\'objet-Nom\']').val();

                if (objetNom !== '') {
                    console.log("Recherche de " + objetNom + " de type " + objetType);

                    if (objetType == "items") {
                        game.items.forEach(t => {
                            if (t.name == objetNom) {
                                console.log(t);
                            }
                        });
                    }

                    if (objetType == "actors") {
                        game.actors.forEach(t => {
                            if (t.name == objetNom) {
                                console.log(t);
                            }
                        });
                    }

                    if (objetType == "folders") {
                        game.folders.forEach(t => {
                            if (t.name == objetNom) {
                                console.log(t);
                            }
                        });
                    }
                }
            }
        }).render(true);
    }
}
