import {Tor1eCompendiumCaracteristiques} from "./scripts/caracteristiques.js";
import {Tor1eCompendiumEquipement} from "./scripts/equipement.js";
import {Tor1eCompendiumAdversaires} from "./scripts/adversaires.js";
import {Tor1eCompendiumPNJ} from "./scripts/pnj.js";
import {Tor1eCompendiumDev} from "./scripts/dev.js";

export class Tor1eCompendium {
    constructor() {
        this.mesCaracteristiques = new Tor1eCompendiumCaracteristiques();
        this.monEquipement = new Tor1eCompendiumEquipement();
        this.mesAdversaires = new Tor1eCompendiumAdversaires();
        this.mesPNJs = new Tor1eCompendiumPNJ();
        this.dev = new Tor1eCompendiumDev();
    }

    // INITIALISATION OF CHARACTER CARACTERISTICS
    async initCaracteristiques() {
        console.log("=== Tor1eCompendium : CARAC DEBUT ===");
        await this.mesCaracteristiques.creationCaracteristiques();
        console.log("=== Tor1eCompendium : CARAC FIN ===");
    }

    // INTIALISATION OF EQUIPMENT ITEMS
    async initEquipement() {
        console.log("=== Tor1eCompendium : EQUIPEMENT DEBUT ===");
        await this.monEquipement.creationEquipement();
        console.log("=== Tor1eCompendium : EQUIPEMENT FIN ===");
    }
 
    // INITIALISATION OF ADVERSARIES
    async initAdversaires() {
        console.log("=== Tor1eCompendium : ADVERSAIRES DEBUT ===");
        await this.mesAdversaires.creationAdversaires();
        console.log("=== Tor1eCompendium : ADVERSAIRES FIN ===");
    }

    // INITIALISATION OF PNJs
    async initPNJs() {
        console.log("=== Tor1eCompendium : PNJ DEBUT ===");
        await this.mesPNJs.creationPNJs();
        console.log("=== Tor1eCompendium : PNJ FIN ===");
    }

    // INITIALISATION OF THE ENTIRE WORLD (caracteristics, items, adversaries)
    async initWorld() {
        ui.notifications.notify('=== TOR1E - FR - COMPENDIUM : DEBUT ===');
        await this.initCaracteristiques();
        await this.initEquipement();
        await this.initAdversaires();
        await this.initPNJs();
        ui.notifications.notify('=== TOR1E - FR - COMPENDIUM : FIN ===');
    }

    // DELETE ALL ACTORS, ITEMS, FOLDERS (after choice)
    async dev_Delete_all() {
        await this.dev.dev_Delete_all();
    }

    // EXPORT AN ACTOR / ITEM / FOLDER TO JSON FILE
    async dev_ExportActor_to_Json() {
        await this.dev.dev_ExportActor_to_Json();
    }

    // SEND AN ACTOR / ITEM / FOLDER TO THE CONSOLE
    async dev_Debug() {
        await this.dev.dev_Debug();
    }
}

Hooks.once('init', async function () {
    game.tor1eCompendium = new Tor1eCompendium();
    console.log("=== Tor1eCompendium : Init ok===");
});
